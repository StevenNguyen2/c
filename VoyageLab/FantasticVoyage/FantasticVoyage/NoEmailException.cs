﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasticVoyage
{
    class NoEmailException : ApplicationException
    {
        public NoEmailException() : base("No Email")
        {

        }
    }
}
