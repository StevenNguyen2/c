﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasticVoyage
{
    class EmailsDontMatchException : ApplicationException
    {
        public EmailsDontMatchException() : base("Emails don't match")
        {

        }
    }
}
