﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasticVoyage
{
    class NoConfirmEmailException : ApplicationException
    {
        public NoConfirmEmailException() : base("Email isn't confirmed")
        {

        }
    }
}
