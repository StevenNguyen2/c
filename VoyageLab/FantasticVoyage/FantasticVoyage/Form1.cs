﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FantasticVoyage
{
    public partial class Form1 : Form
    {
        string destination;
        string quoteMoney;
        string picture;

        public Form1()
        {
            InitializeComponent();
        }

        private void rbBahamas_CheckedChanged(object sender, EventArgs e)
        {
            pictureBox1.ImageLocation = "..\\..\\images\\bahamas.jpg";
        }

        private void rbParis_CheckedChanged(object sender, EventArgs e)
        {
            pictureBox1.ImageLocation = "..\\..\\images\\paris.jpg";
        }

        private void rbItaly_CheckedChanged(object sender, EventArgs e)
        {
            pictureBox1.ImageLocation = "..\\..\\images\\italy.jpg";
        }

        private void rbFlorida_CheckedChanged(object sender, EventArgs e)
        {
            pictureBox1.ImageLocation = "..\\..\\images\\florida.jpg";
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                btnSubmit.Enabled = true;
            }
            else
            {
                btnSubmit.Enabled = false;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            btnSubmit.Enabled = false;
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (tbFirstName.Text != "")
                {
                    label5.Text = "";
                }
                else
                {
                    throw new NoFirstNameException();
                }
            
                if (tbLastName.Text != "")
                {
                    label8.Text = "";
                }
                else
                {
                    throw new NoLastNameException();
                }
            
                if (tbNoEmail.Text != "")
                {
                    label9.Text = "";
                }
                else
                {
                    throw new NoEmailException();
                }
            
                if (tbNoConfirm.Text != "")
                {
                    label10.Text = "";
                }
                else
                {
                    throw new NoConfirmEmailException();
                }
            
                if (tbNoEmail.Text == tbNoConfirm.Text)
                {
                    label10.Text = "";
                }
                else
                {
                    throw new EmailsDontMatchException();
                }

                //PRINT TO HTML DOC
                string firstName = tbFirstName.Text;
                string lastName = tbLastName.Text;
                string email = tbNoConfirm.Text;
                picture = Convert.ToString(pictureBox1);

                if (rbBahamas.Checked)
                {
                    picture = "C:\\Users\\Steven Nguyen\\Desktop\\Customer Quotes\\bahamas.jpg";
                    quoteMoney = " $1899.00";
                    destination = " Bahamas";
                }
                else if (rbParis.Checked)
                {
                    picture = "C:\\Users\\Steven Nguyen\\Desktop\\Customer Quotes\\paris.jpg";
                    quoteMoney = " $1999.00";
                    destination = " Paris";

                }
                else if (rbItaly.Checked)
                {
                    picture = "C:\\Users\\Steven Nguyen\\Desktop\\Customer Quotes\\italy.jpg";
                    quoteMoney = " $1799.00";
                    destination = " Italy";

                }
                else if (rbFlorida.Checked)
                {
                    picture = "C:\\Users\\Steven Nguyen\\Desktop\\Customer Quotes\\florida.jpg";
                    quoteMoney = " $1200.00";
                    destination = " Florida";

                }

                StreamWriter sWrite = new StreamWriter("C:\\Users\\Steven Nguyen\\Desktop\\Customer Quotes\\fantasticvoyagetravel_" + firstName + "_" + lastName + "_quote.html");
                //StreamWriter sWrite = new StreamWriter("C:\\Users\\Steven Nguyen\\Desktop\\Customer Quotes\\test.html");

                sWrite.WriteLine("<html>");
                sWrite.WriteLine("<body>");
                sWrite.WriteLine("<div style=width:600px; margin-left: auto; margin-right: auto;>");
                sWrite.WriteLine("<h1 style=border:solid><center>Fantastic Voyage Travel Agency</center></h1>");
                sWrite.WriteLine("<h3>Destination:" + destination + "</h3>");
                sWrite.WriteLine("<h3>Quote:" + quoteMoney + "</h3>");
                sWrite.WriteLine("<h3>Customer Info:</h3>");
                sWrite.WriteLine("<div>" + firstName + " " + lastName + "</div>");
                sWrite.WriteLine("<a href='mailto:" + email + "'>" + email + "</a>");
                sWrite.WriteLine("<p></p>");
                sWrite.WriteLine("<div style= float: 'right'>");
                sWrite.WriteLine("<img src = '" + picture + "' alt = 'Vibes' style= align: 'right';");
                sWrite.WriteLine("</div>");
                sWrite.WriteLine("</div>");
                sWrite.WriteLine("</body>");
                sWrite.WriteLine("</html>");
                sWrite.Close();
            }
            catch (NoFirstNameException ex)
            {
                label5.Text = ex.Message;
            }
            catch (NoLastNameException ex)
            {
                label8.Text = ex.Message;
            }
            catch (NoEmailException ex)
            {
                label9.Text = ex.Message;
            }
            catch (NoConfirmEmailException ex)
            {
                label10.Text = ex.Message;
                return;
            }
            catch (EmailsDontMatchException ex)
            {
                label10.Text = ex.Message;
                return;
            }
        }
    }
}