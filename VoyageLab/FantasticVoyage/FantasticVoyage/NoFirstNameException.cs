﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasticVoyage
{
    class NoFirstNameException : ApplicationException
    {
        public NoFirstNameException() : base("No First Name")
        {

        }
    }
}
