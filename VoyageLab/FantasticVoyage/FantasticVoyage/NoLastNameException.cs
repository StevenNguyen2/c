﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FantasticVoyage
{
    class NoLastNameException : ApplicationException
    {
        public NoLastNameException() : base("No Last Name")
        {

        }
    }
}
