﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TilingDemo
{
//    Create a program named TilingDemo that instantiates an array of 10 Room
//objects and demonstrates its methods.The Room constructor requires
//parameters for length and width fields; use a variety of values when constructing
//the objects.The Room class also contains a field for floor area of the Room
//and number of boxes of tile needed to tile the room.Both of these values are
//computed by calling private methods.A room requires one box of tile for
//every 12 full square feet plus a box for any partial square footage over 12, plus
//one extra box for waste from irregular cuts.In other words, a 122 square foot
//room requires 12 boxes—10 boxes for the first 120 square feet, 1 box for the
//2 extra square feet over 120, and 1 box for waste.Include read-only properties to
//get a Room’s values.
    class Room
    {
        int[] length;
        int[] width;
        int floorArea;
        int numberOfBoxes;

        public Room(int[] array1, int[] array2)
        {
            this.length = new int[array1.Length];
            this.width = new int[array2.Length];
            for (int i = 0; i < array1.Length; i++)
            {
                this.length[i] = array1[i];
                this.width[i] = array2[i];
            }
            GetFloorArea();
        }//End constructor

        private void GetFloorArea()
        {
            for (int i = 0; i < length.Length; i++)
            {
                //numberOfBoxes = 0; //reset boxes to 0
                floorArea = length[i] * width[i];
                GetNumberOfBoxes(floorArea);
                //if (floorArea % 12 == 0)
                //{
                //    //evenly divisible by 12
                //    numberOfBoxes += 1; //add one for irregular cuts
                //    numberOfBoxes += floorArea / 12;
                //    WriteLine("# of boxes: " + numberOfBoxes);
                //}
                //else
                //{
                //    numberOfBoxes += 2; //add one for irregular cuts + add another one because it's greater than 12 square feet
                //    numberOfBoxes += floorArea / 12;
                //    WriteLine("# of boxes: " + numberOfBoxes);
                //}
            }
        }

        private void GetNumberOfBoxes(int area)
        {
            numberOfBoxes = 0;
            if (floorArea % 12 == 0)
            {
                //evenly divisible by 12
                numberOfBoxes += 1; //add one for irregular cuts
                numberOfBoxes += floorArea / 12;
                WriteLine("# of boxes: " + numberOfBoxes);
            }
            else
            {
                numberOfBoxes += 2; //add one for irregular cuts + add another one because it's greater than 12 square feet
                numberOfBoxes += floorArea / 12;
                WriteLine("# of boxes: " + numberOfBoxes);
            }
        }
    }
}
