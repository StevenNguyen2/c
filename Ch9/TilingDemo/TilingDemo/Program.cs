﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TilingDemo
{
    class Program
    {
//        Create a program named TilingDemo that instantiates an array of 10 Room
//objects and demonstrates its methods.The Room constructor requires
//parameters for length and width fields; use a variety of values when constructing
//the objects.The Room class also contains a field for floor area of the Room
//and number of boxes of tile needed to tile the room.Both of these values are
//computed by calling private methods.A room requires one box of tile for
//every 12 full square feet plus a box for any partial square footage over 12, plus
//one extra box for waste from irregular cuts.In other words, a 122 square foot
//room requires 12 boxes—10 boxes for the first 120 square feet, 1 box for the
//2 extra square feet over 120, and 1 box for waste.Include read-only properties to
//get a Room’s values.
        static void Main(string[] args)
        {
            int[] length = { 6, 4, 12, 5, 6, 7, 8, 9, 10, 11 };
            int[] width = { 2, 3, 11, 5, 8, 9, 4, 10, 12, 13 };
            Room demo1 = new Room(length, width);
            ReadLine();
        }
    }
}
