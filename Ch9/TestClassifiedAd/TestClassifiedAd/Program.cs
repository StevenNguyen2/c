﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestClassifiedAd
{
    class Program
    {
        //Create an application named TestClassifiedAd that instantiates and displays a ClassifiedAd object. 
        //A ClassifiedAd has fields for a category(for example, Used Cars), a number of words, and a price.
        //Include properties that contain get and set accessors for the category and number of words, but only a get accessor for the price. 
        //The price is calculated at nine cents per word.
        static void Main(string[] args)
        {
            ClassifiedAd ad1 = new ClassifiedAd("used cars", 10);
            ad1.DisplayPrice();

            ClassifiedAd ad2 = new ClassifiedAd("used cars", 10);
            ad2.Category = "new cars";
            ad2.DisplayPrice();
        }
    }
}
