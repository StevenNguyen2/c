﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestClassifiedAd
{
    class ClassifiedAd
    {
        //Create an application named TestClassifiedAd that instantiates and displays a ClassifiedAd object. 
        //A ClassifiedAd has fields for a category(for example, Used Cars), a number of words, and a price.
        //Include properties that contain get and set accessors for the category and number of words, but only a get accessor for the price. 
        //The price is calculated at nine cents per word.

        string category;
        int numberOfWords;
        double price = 0.09;

        public string Category
        {
            get
            {
                return category;
            }
            set
            {
                this.category = value;
            }
        }

        public int NumberOfWords
        {
            get
            {
                return numberOfWords;
            }
            set
            {
                this.numberOfWords = value;
            }
        }

        public double Price
        {
            get
            {
                return price;
            }
        }

        public ClassifiedAd(string category, int numberOfWords)
        {
            this.category = category;
            this.numberOfWords = numberOfWords;
            price = numberOfWords * price;
        }

        public void DisplayPrice()
        {
            string results = "";
            results = "An ad about " + category + " with " + numberOfWords + " word(s) would cost " + price.ToString("C");
            WriteLine(results);
            ReadLine();
        }
    }
}
