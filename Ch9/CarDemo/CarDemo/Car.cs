﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarDemo
{
//    Create an application named CarDemo that declares at least two Car objects and
//demonstrates how they can be incremented using an overloaded ++ operator. Create
//a Car class that contains a model and a value for miles per gallon.Include two
//overloaded constructors.One accepts parameters for the model and miles per gallon;
//    the other accepts a model and sets the miles per gallon to 20. Overload a ++ operator
//    that increases the miles per gallon value by 1. The CarDemo application creates at
//    least one Car using each constructor and displays the Car values both before and
//    after incrementation.
    class Car
    {
        public string model;
        public double milesPerGallon { get; set; }

        public Car(string model, double milesPerGallon)
        {
            this.model = model;
            this.milesPerGallon = milesPerGallon;
        }

        public Car(string model)
        {
            this.model = model;
            this.milesPerGallon = 20;
        }
        
        //public void CalculateMPG()
        //{
        //    WriteLine(model + "'s MPG before: " + milesPerGallon);
        //    milesPerGallon++;
        //    WriteLine(model + "'s MPG after: " + milesPerGallon);
        //}

        public static Car operator++(Car MPG)
        {
            MPG.milesPerGallon++;
            return MPG;
        }
    }
}
