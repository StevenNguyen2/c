﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarDemo
{
    class Program
    {
//        Create an application named CarDemo that declares at least two Car objects and
//demonstrates how they can be incremented using an overloaded ++ operator. Create
//a Car class that contains a model and a value for miles per gallon.Include two
//overloaded constructors.One accepts parameters for the model and miles per gallon;
//    the other accepts a model and sets the miles per gallon to 20. Overload a ++ operator
//    that increases the miles per gallon value by 1. The CarDemo application creates at
//    least one Car using each constructor and displays the Car values both before and
//    after incrementation.
        static void Main(string[] args)
        {
            Car chevrolet = new Car("chevrolet", 10);
            Car lexus = new Car("lexus");

            WriteLine(chevrolet.model + "'s MPG before: " + chevrolet.milesPerGallon);
            ++chevrolet;
            WriteLine(chevrolet.model + "'s MPG after: " + chevrolet.milesPerGallon);

            WriteLine(lexus.model + "'s MPG before: " + lexus.milesPerGallon);
            lexus++;
            WriteLine(lexus.model + "'s MPG after: " + lexus.milesPerGallon);
            ReadLine();
        }
    }
}
