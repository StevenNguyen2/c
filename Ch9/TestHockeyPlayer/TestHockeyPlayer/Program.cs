﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestHockeyPlayer
{
    //Create an application named TestHockeyPlayer that instantiates and displays a HockeyPlayer object. 
    //The HockeyPlayer class contains fields for a player’s name(a string), jersey number(an integer), and goals scored(an integer).
    class Program
    {
        static void Main(string[] args)
        {
            HockeyPlayer player1 = new HockeyPlayer("Tarasenko", 91, 40); //Instantiate a new object (player1 in this instance) and setting a Name, Jersey, and GoalsScored Property w/ default constructor
            player1.ScoreGoal(50); //Calling an object's (player1 in this instance) method & passing in value of 50
            string name = player1.Name; //Called an object's (player1 in this instance) property (Name) & retrieved the value by setting it to string name (get). Object is on the right of the equal sign so we're getting something
            player1.DisplayDetails();

            player1.Name = "Pietrangelo"; //Setting the property (Name) to Pietrangelo (set). Object is on the left of the equal sign so we're setting it to something
            player1.Jersey = 27;
            player1.Goals_Scored = 0;
            player1.DisplayDetails();

            ReadLine();
        }
    }
}