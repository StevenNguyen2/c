﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestHockeyPlayer
{
    //Create an application named TestHockeyPlayer that instantiates and displays a HockeyPlayer object. 
    //The HockeyPlayer class contains fields for a player’s name(a string), jersey number(an integer), and goals scored(an integer).
    class HockeyPlayer
    {
        private string name;
        private int jersey;
        private int goalsScored;

        public string Name
        {
            //Creating property (Name) and adding getters & setters
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public int Jersey
        {
            get
            {
                return jersey;
            }
            set
            {
                jersey = value;
            }
        }

        public int Goals_Scored
        {
            get
            {
                return goalsScored;
            }
            set
            {
                goalsScored = value;
            }
        }

        public HockeyPlayer(string name, int jersey, int goalsScored)
        {
            this.name = name;
            this.jersey = jersey;
            this.goalsScored = goalsScored;
        }//End constructor

        public void DisplayDetails()
        {
            string results = "";
            results = "Player " + name + ", jersey number " + jersey + ", has " + goalsScored + " goals scored.";
            WriteLine(results);
        }

        public void ScoreGoal(int numGoals)
        {
            //Takes in a number (numGoals) and adds numGoals to the property (goalsScored). (Creating a method that will do some kind of math)
            goalsScored += numGoals;
        }
    }
}
