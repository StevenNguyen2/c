﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxPayer2
{
    class TaxPayer : IComparable
    {
//        Create a program named TaxpayerDemo2 so that after the 10 Taxpayer objects
//are displayed, they are sorted in order by the amount of tax owed and displayed
//again.Modify the Taxpayer class so its objects are comparable to each other
//based on tax owed
        int iteration;
        string socialSecurity;
        double grossIncome;
        double incomeTaxAmount;

        public string SocialSecurity
        {
            get
            {
                return socialSecurity;
            }
            set
            {
                socialSecurity = value;
            }
        }

        public double GrossIncome
        {
            get
            {
                return grossIncome;
            }
            set
            {
                grossIncome = value;
            }
        }

        public double IncomeTax
        {
            get
            {
                return incomeTaxAmount;
            }
        }

        public TaxPayer(string socialSecurity, double grossIncome, int iteration)
        {
            this.socialSecurity = socialSecurity;
            this.grossIncome = grossIncome;
            this.iteration = iteration;
            CalculateIncome(this.grossIncome);
        }

        private void CalculateIncome(double grossIncomeAmount)
        {
            if (grossIncomeAmount >= 30000)
            {
                incomeTaxAmount = grossIncomeAmount * .28;
                //result = incomeTax -(grossIncome * incomeTax);
                WriteLine("Payer " + iteration + " has a net income of " + incomeTaxAmount);
            }
            else
            {
                incomeTaxAmount = grossIncomeAmount * .15;
                //result = grossIncome * incomeTax;
                WriteLine("Payer " + iteration + " has a net income of " + grossIncomeAmount);
            }
        }

        public int CompareTo(object obj)
        {
            TaxPayer other = (TaxPayer)obj;
            return incomeTaxAmount.CompareTo(other.incomeTaxAmount);
        }
    }
}