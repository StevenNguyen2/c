﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxPayer2
{
    class Program
    {
        //        Create a program named TaxpayerDemo2 so that after the 10 Taxpayer objects
        //are displayed, they are sorted in order by the amount of tax owed and displayed
        //again.Modify the Taxpayer class so its objects are comparable to each other
        //based on tax owed
        static void Main(string[] args)
        {
            string socialSecurity;
            double grossIncome;
            int payerIteration;

            TaxPayer[] taxArray = new TaxPayer[10];
            for (int i = 0; i < taxArray.Length; i++)
            {
                Write("Enter Social Security Number: ");
                socialSecurity = ReadLine();
                Write("Enter Gross Income: ");
                grossIncome = Convert.ToDouble(ReadLine());
                payerIteration = i + 1;
                TaxPayer payer = new TaxPayer(socialSecurity, grossIncome, payerIteration);
                taxArray[i] = payer;
            }
            Array.Sort(taxArray);
            WriteLine("Sorted:");
            for (int i = 0; i < taxArray.Length; i++)
            {
                WriteLine(taxArray[i].IncomeTax);
            }
            ReadLine();
        }
    }
}
