﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transaction
{
    class Class
    {
        static void Main(string[] args)
        {
            Burger burger1 = new Burger(new string[] { "Onions", "Pickles", "Mayo", "Tomato" });
            burger1.DisplayDetails();
            Burger burger2 = new Burger();
            burger2.DisplayDetails();

            Console.ReadLine();
        }
    }
}
