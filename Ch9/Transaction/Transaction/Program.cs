﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transaction
{
    class Burger
    {
        private string[] noneToppings; //The toppings the customer doesn't want
        public const double BASE_PRICE = 2.30; //Base price of a Burger 

        public Burger()
        {
            this.noneToppings = new string[0]; 
        }//End constructor

        public Burger(string[] noneToppings)
        {
            this.noneToppings = new string[noneToppings.Length];
            for (int i = 0; i < noneToppings.Length; ++i)
            {
                this.noneToppings[i] = noneToppings[i]; //set the field values
            }//End for
        }//End constructor

        public string[] NoneToppings
        {
            get
            {
                return noneToppings;
            }//End getter
            set
            {
                this.noneToppings = new string[value.Length];
                for (int i = 0; i < noneToppings.Length; ++i)
                {
                    this.noneToppings[i] = value[i]; //set the field values
                }//End for
            }//End setter
        }//End property

        public double BasePrice
        {
            get
            {
                return BASE_PRICE - (noneToppings.Length * .1);
            }//End get
        }//End property

        public void DisplayDetails()
        {
            string details = "";
            if(noneToppings.Length == 0)
            {
                details += "1 Reg Burger" + BasePrice.ToString("C");
            }
            else
            {
                details += "1 Custom Burger" + BasePrice.ToString("C");
                for(int i = 0; i < noneToppings.Length; ++i)
                {
                    details += "\n\t-" + noneToppings[i];
                }
            }

            details += "\n\n";
           Console.Write(details);
        }
    }//End class
}//End namespace
