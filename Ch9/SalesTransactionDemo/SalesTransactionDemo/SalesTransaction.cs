﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesTransactionDemo
{
//    Create an application named SalesTransactionDemo that declares several
//SalesTransaction objects and displays their values and their sum.
//The SalesTransaction class contains fields for a salesperson name, sales amount,
//and commission and a readonly field that stores the commission rate.Include
//three constructors for the class. One constructor accepts values for the name, sales
//amount, and rate, and when the sales value is set, the constructor computes the
//commission as sales value times commission rate.The second constructor accepts
//a name and sales amount, but sets the commission rate to 0. The third constructor
//accepts a name and sets all the other fields to 0. An overloaded + operator adds the
//sales values for two SalesTransaction objects
    public class SalesTransaction
    {
        private string _salespersonName;
        private double _salesAmount;
        private readonly double _commissionRate;
        private double _commissionAmount;



        public SalesTransaction(string salespersonName, double salesAmount, double commissionRate)
        {
            this._salespersonName = salespersonName;
            this._salesAmount = salesAmount;
            this._commissionRate = commissionRate;
            _commissionAmount = _salesAmount * _commissionRate;
        }

        public SalesTransaction(string salespersonName, double salesAmount) : this (salespersonName, salesAmount, 0)
        {
            //this._salespersonName = salespersonName;
            //this._salesAmount = salesAmount;
            //_commission = 0.0;
        }

        public SalesTransaction(string salespersonName) : this (salespersonName, 0, 0)
        {
            //this._salespersonName = salespersonName;
            //_salesAmount = 0;
            //_commission = 0.0;
        }

        public static double operator+(SalesTransaction firstSale, SalesTransaction secondSale)
        {
            double salesTotal = firstSale._salesAmount + secondSale._salesAmount;
            return salesTotal;
        }

        public void ShowMath()
        {
            WriteLine("Sales Person Name: " + _salespersonName);
            WriteLine("Sales Amount: " + _salesAmount.ToString("C"));
            WriteLine("Commission Rate: " + _commissionRate.ToString("P"));
            double commissionEarned;
            commissionEarned = _salesAmount * _commissionRate;

            if (_commissionRate > 0)
            {
                WriteLine(_salespersonName + "'s total commission earned: " + commissionEarned.ToString("C"));
            }
            else
            {
                WriteLine(_salespersonName + " has no commission");
            }
        }
    }
}
