﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesTransactionDemo
{
    class Program
    {
//        Create an application named SalesTransactionDemo that declares several
//SalesTransaction objects and displays their values and their sum.
//The SalesTransaction class contains fields for a salesperson name, sales amount,
//and commission and a readonly field that stores the commission rate.Include
//three constructors for the class. One constructor accepts values for the name, sales
//amount, and rate, and when the sales value is set, the constructor computes the
//commission as sales value times commission rate.The second constructor accepts
//a name and sales amount, but sets the commission rate to 0. The third constructor
//accepts a name and sets all the other fields to 0. An overloaded + operator adds the
//sales values for two SalesTransaction objects
        static void Main(string[] args)
        {
            SalesTransaction sales1 = new SalesTransaction("Kevin", 10, 0.5);
            sales1.ShowMath();

            WriteLine("");

            SalesTransaction sales2 = new SalesTransaction("Steven", 5);
            sales2.ShowMath();

            WriteLine("");

            SalesTransaction sales3 = new SalesTransaction("Josh");
            sales3.ShowMath();

            WriteLine("");

            //SalesTransaction sales4 = sales1 + sales2;

            double sales4;
            sales4 = sales1 + sales2;
            WriteLine("Sum of sales1 + sales2: " + sales4.ToString("C"));
            ReadLine();
        }
    }
}
