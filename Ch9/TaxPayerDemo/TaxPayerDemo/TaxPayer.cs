﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxPayerDemo
{
    //        Create a program named TaxPayerDemo that declares an array of 10 Taxpayer
    //objects.Prompt the user for data for each object and display the 10 objects.Data
    //fields for Taxpayer objects include the Social Security number (use a string for
    //the type, but do not use dashes within the Social Security number), the yearly
    //gross income, and the income tax owed.Include a property with get and set
    //accessors for the first two data fields, but make the tax owed a read-only property.
    //The tax should be calculated whenever the income is set.Assume that the tax
    //rate is 15 percent for incomes under $30,000 and 28 percent for incomes that are
    //$30,000 or higher
    class TaxPayer
    {
        int iteration;
        string socialSecurity;
        double grossIncome;
        double incomeTax;

        public string SocialSecurity
        {
            get
            {
                return socialSecurity;
            }
            set
            {
                socialSecurity = value;
            }
        }

        public double GrossIncome
        {
            get
            {
                return grossIncome;
            }
            set
            {
                grossIncome = value;
            }
        }

        public double IncomeTax
        {
            get
            {
                return incomeTax;
            }
        }

        public TaxPayer(string socialSecurity, double grossIncome, int iteration)
        {
            this.socialSecurity = socialSecurity;
            this.grossIncome = grossIncome;
            this.iteration = iteration;
            CalculateIncome(this.grossIncome);
        }

        private void CalculateIncome(double grossIncome)
        {
            double result = 0.0;
            if (grossIncome >= 30000)
            {
                incomeTax = .28;
                result = grossIncome * incomeTax;
                WriteLine("Payer " + iteration + " has a net income of " + result);
            }
            else
            {
                incomeTax = .15;
                result = grossIncome * incomeTax;
                WriteLine("Payer " + iteration + " has a net income of " + result);
            }
        }
    }
}