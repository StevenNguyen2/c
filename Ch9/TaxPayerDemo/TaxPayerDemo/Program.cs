﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxPayerDemo
{
    class Program
    {
//        Create a program named TaxPayerDemo that declares an array of 10 Taxpayer
//objects.Prompt the user for data for each object and display the 10 objects.Data
//fields for Taxpayer objects include the Social Security number (use a string for
//the type, but do not use dashes within the Social Security number), the yearly
//gross income, and the income tax owed.Include a property with get and set
//accessors for the first two data fields, but make the tax owed a read-only property.
//The tax should be calculated whenever the income is set.Assume that the tax
//rate is 15 percent for incomes under $30,000 and 28 percent for incomes that are
//$30,000 or higher
        static void Main(string[] args)
        {
            string socialSecurity;
            double grossIncome;
            int payerIteration;

            TaxPayer[] taxArray = new TaxPayer[10];
            for (int i = 0; i < taxArray.Length; i++)
            {
                Write("Enter Social Security Number: ");
                socialSecurity = ReadLine();
                Write("Enter Gross Income: ");
                grossIncome = Convert.ToDouble(ReadLine());
                payerIteration = i + 1;
                TaxPayer payer = new TaxPayer(socialSecurity, grossIncome, payerIteration);
                taxArray[i] = payer;
            }
            ReadLine();
        }
    }
}
