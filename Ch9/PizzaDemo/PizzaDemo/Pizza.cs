﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaDemo
{
    public class Pizza : Food, Weight //Create a class & inherit Food class/Weight interface
    {
        private string _crustType; //Create instance fields

        public Pizza() //default constructor
        {
            WriteLine("Default constructor has been called");
            _crustType = "Thin Crust";
            double basePrice = 5.00; //Base price for every pizza (local variable)
            Price = basePrice + 4.99; //Current price of the pizza
        }
        public Pizza(string crustType) //Create a constructor
        {
            WriteLine("String constructor has been called");
            this._crustType = crustType; //use this to reference the instance field of crustType
            double basePrice = 5.00; //Base price for every pizza
            Price = basePrice + 4.99; //Current price of the pizza
        }

        public string CrustType //Create properties
        {
            get
            {
                return _crustType;
            }
            set
            {
                _crustType = value;
            }
        }

        public double Price { get; set; } //Create auto-implemented properties

        public void PutPizzaInOven(string crustType) //Create instance methods
        {
            if(crustType == "Thin Crust")
                WriteLine("The thin crust pizza has been placed in the oven for five minutes");
            if (crustType == "Hand-Tossed")
                WriteLine("The hand-tossed pizza has been placed in the oven for ten minutes");
        }

        public void CompletePurchase() //Create instance methods
        {
            WriteLine("The order has been completed");
        }

        public override string ToString() //override methods
        {
            string stringBuilder = "";
            stringBuilder = "Pizza Order\n********\nCrust Type: " + CrustType + "\nPrice: " + Price.ToString("C");
            return stringBuilder;
        }

        public override void EatFood()
        {
            WriteLine("The pizza is being eaten");
        }

        public override void PrepareFood()
        {
            WriteLine("The food is being prepared");
        }

        public double GetWeight()
        {
            return Weight;
        }

        public void SetWeight(double num)
        {
            Weight = num;
        }
    }
}
