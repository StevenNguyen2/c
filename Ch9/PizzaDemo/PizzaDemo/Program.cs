﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Pizza pizza = new Pizza(); //Instantiate objects //references first constructor
            Pizza pizza2 = new Pizza("Hand-Tossed"); //references second constructor

            WriteLine("Pizza2 crust type is : " + pizza2.CrustType); //getter and setter allows us to do this
            WriteLine();
            pizza2.CrustType = "thin-crust";
            WriteLine();
            WriteLine("Pizza2 crust type is : " + pizza2.CrustType);
            WriteLine();
            WriteLine("Pizza1 price is : " + pizza.Price.ToString("C"));
            WriteLine();
            WriteLine("Pizza2 price is : " + pizza2.Price.ToString("C"));
            pizza.Price = 5.99;
            WriteLine();
            WriteLine("Pizza1 is : " + pizza.Price.ToString("C"));
            WriteLine();
            pizza.CompletePurchase();
            pizza.PrepareFood();
            pizza.PutPizzaInOven("Thin Crust"); //passing values to methods
            pizza2.CompletePurchase();
            pizza2.PrepareFood();
            pizza2.PutPizzaInOven("Hand-Tossed"); //passing values to methods
            WriteLine();
            WriteLine(pizza2.ToString()); //returning values to methods
            pizza2.EatFood();
            pizza2.Name = "Pizza"; //purpose is to show you can still reference other properties cause Food is a child of Pizza class

            ReadLine();
        }
    }
}
