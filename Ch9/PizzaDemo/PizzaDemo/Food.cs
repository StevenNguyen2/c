﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaDemo
{
    public abstract class Food
    {
        public string Name { get; set; }
        public int Calories { get; set; }
        public double Weight { get; set; }

        public virtual void EatFood() //override methods
        {
            WriteLine("The food is being eaten");
        }

        public abstract void PrepareFood(); //Create abstract class
    }
}
