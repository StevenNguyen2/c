﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConferencesDemo
{
    class Conference : IComparable
    {
        private string groupName;
        private string startingDate;
        private int numOfAttendees;

        public string GroupName
        {
            get
            {
                return groupName;
            }
            set
            {
                groupName = value;
            }
        }

        public string StartingDate
        {
            get
            {
                return startingDate;
            }
            set
            {
                startingDate = value;
            }
        }

        public int NumberOfAttendees
        {
            get
            {
                return numOfAttendees;
            }
            set
            {
                numOfAttendees = value;
            }
        }

        //public Conference()
        //{
              //Constructor
        //}

        public int CompareTo(object obj)
        {
            Conference other = (Conference)obj;
            return numOfAttendees.CompareTo(other.numOfAttendees);
        }
    }
}
