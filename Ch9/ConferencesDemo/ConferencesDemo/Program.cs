﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConferencesDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Conference[] array = new Conference[5];

            for (int i = 0; i < array.Length; i++)
            {
                WriteLine("Enter Group Name:");
                array[i] = new Conference { GroupName = ReadLine() };
                WriteLine("Enter Starting Date:");
                array[i].StartingDate = ReadLine();
                WriteLine("Enter Number of Attendees:");
                array[i].NumberOfAttendees = Convert.ToInt32(ReadLine());
            }
            Array.Sort(array);
            //Array.Reverse(array);
            for (int i = 0; i < array.Length; i++)
            {
                WriteLine("Group " + array[i].GroupName + " has an attendance of: " + array[i].NumberOfAttendees); 
            }
            ReadLine();
        }
    }
}
