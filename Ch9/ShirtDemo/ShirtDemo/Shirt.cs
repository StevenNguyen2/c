﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShirtDemo
{
    class Shirt
    {
        //Create an application named ShirtDemo that declares several Shirt objects and includes a display method to
        //which you can pass different numbers of Shirt objects in successive method calls.The Shirt class contains 
        //auto-implemented properties for a material, color, and size.

        //auto-implemented properties
        public string _material { get; set; }
        public string _color { get; set; }
        public string _size { get; set; }

        //public string DisplayShirt()
        //{
        //    string shirts;
        //    shirts = material + " " + color + " " + size;
        //    WriteLine(shirts);
        //}

        //constructor
        public Shirt(string material, string color, string size)
        {
            this._material = material;
            this._color = color;
            this._size = size;
        }

        public static void DisplayShirt(params Shirt[] shirts)
        {
            //string str = "";
            for (int i = 0; i < shirts.Length; i++)
            {
                WriteLine("Shirt's material: " + shirts[i]._material);
                WriteLine("Shirt's color: " + shirts[i]._color);
                WriteLine("Shirt's size: " + shirts[i]._size);
            }
            //return str;
        }
    }
}
