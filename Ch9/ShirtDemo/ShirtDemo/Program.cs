﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShirtDemo
{
    class Program
    {
        //Create an application named ShirtDemo that declares several Shirt objects and includes a display method to
        //which you can pass different numbers of Shirt objects in successive method calls.The Shirt class contains 
        //auto-implemented properties for a material, color, and size.
        static void Main(string[] args)
        {
            //Shirt shirt1 = new Shirt()
            //{
            //    material = "cotton";
            //    color = "red";
            //    size = "small";
            //}

            Shirt shirt1 = new Shirt("Cotton", "Red", "Small");
            Shirt shirt2 = new Shirt("Cashmere", "Blue", "Medium");
            Shirt shirt3 = new Shirt("Linen", "Black", "Large");

            Shirt.DisplayShirt(shirt1);
            WriteLine("");
            Shirt.DisplayShirt(shirt2);
            WriteLine("");
            Shirt.DisplayShirt(shirt3);

            ReadLine();
        }
    }
}
