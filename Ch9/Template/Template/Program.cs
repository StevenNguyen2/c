﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Template
{
    class Program
    {
        static void Main(string[] args)
        {
            //Asking a user for input & validating
            WriteLine("Enter a number greater than 0");
            int userInput;
            Int32.TryParse(ReadLine(), out userInput); //TryParse will set userInput to 0 if failed
            while (userInput == 0) //TryParse will set userInput to w/e user enters
            {
                WriteLine("Enter a valid number");
                Int32.TryParse(ReadLine(), out userInput);
            }

            WriteLine("Number successfully submitted: " + userInput);
            ReadLine();
        }
    }
}
