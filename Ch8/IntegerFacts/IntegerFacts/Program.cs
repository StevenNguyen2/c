﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegerFacts
{
    class Program
    {
        //Create a program named IntegerFacts whose Main() method declares an array of 20 integers.
        //Call a method to interactively fill the array with any number of values up to 20 or until a sentinel value is entered.
        //If an entry is not an integer, reprompt the user.
        //Call a second method that accepts out parameters for the highest value in the array, lowest value in the array, sum of the values in the array, and arithmetic average.
        //In the Main() method, display all the statistics.
        static void Main(string[] args)
        {
            int[] integer = new int[20];
            FillArray(integer);
            OutParameter(integer, out int highest, out int lowest, out int sum, out int avg);
            WriteLine("Highest: " + highest);
            WriteLine("Lowest: " + lowest);
            WriteLine("Sum: " + sum);
            WriteLine("Average: " + avg);
            ReadLine();
        }

        private static void FillArray(int[] integers)
        {
            int userInput;

            //userInput = Convert.ToInt32(ReadLine());
                   for (int i = 0; i < integers.Length; i++)
                    {
                        WriteLine("Enter a number " + (i + 1) +" or 999 to finish");
                        if (!Int32.TryParse(ReadLine(), out userInput))
                        {
                        WriteLine("Enter a valid integer or 999 to finish");
                        userInput = Convert.ToInt32(ReadLine());
                        }
                        if (userInput != 999)
                        {
                        integers[i] = userInput;
                        }
                        else
                        {
                        i = 21;
                        }
                        //userInput = Convert.ToInt32(ReadLine());
                        //integers[i] = userInput;
                        //userInput = Convert.ToInt32(ReadLine());//
                    }
        }

        private static void OutParameter(int[] intergers2, out int highest, out int lowest, out int sum, out int avg)
        {
            highest = Convert.ToInt32(intergers2.Max());
            lowest = Convert.ToInt32(intergers2.Min());
            sum = Convert.ToInt32(intergers2.Sum());
            avg = Convert.ToInt32(intergers2.Average());
        }
    }
}
