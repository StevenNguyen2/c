﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodOrder
{
    public class FoodOrder
    {
        private IList<Item> items = new List<Item>(0);

        public static void Main()
        {
            FoodOrder order = new FoodOrder();
            Item item = order.Search();
            if (item != null) //if / else loop to determine if item is there or not
            {
                Console.WriteLine(item);
            }
            else
            {
                Console.WriteLine("item not found");
            }

            Console.ReadLine();
        }

        public FoodOrder()
        {
            items = BuildItem(); //constructs the items
        }

        public Item Search()
        {
            Item item = null;
            Console.WriteLine("Enter item number or description");
            string input = Console.ReadLine();
            try //orders the GetDetails methods
            {
                int itemNum = Convert.ToInt32(input);
                item = GetDetails(itemNum);
            }
            catch
            {
                item = GetDetails(input);
            }
            return item;
        }

        public Item GetDetails(string description) //GetDetails method to determine string if typed
        {
            Item item = null;
            foreach (Item i in this.items)
            {
                if (i.Description.Equals(description))
                {
                    item = i;
                    break;
                }
            }
            return item;
        }

        public Item GetDetails(int itemNum) //GetDetails method to determine int if typed
        {
            Item item = null;
            foreach (Item i in this.items)
            {
                if (i.ItemNum == itemNum)
                {
                    item = i;
                    break;
                }
            }
            return item;
        }

        private static IList<Item> BuildItem()
        {
            IList<Item> items = new List<Item>(0); //items are added
            items.Add(new Item { ItemNum = 20, Description = "Enchilada", Price = 2.95 });
            items.Add(new Item { ItemNum = 23, Description = "Burrito", Price = 1.95 });
            items.Add(new Item { ItemNum = 25, Description = "Taco", Price = 2.25 });
            items.Add(new Item { ItemNum = 31, Description = "Tostada", Price = 3.10 });
            return items;
        }
    }

    public class Item
    {
        public int ItemNum { get; set; } //gets and sets the itemNum, Description, and Price
        public string Description { get; set; }
        public double Price { get; set; }

        public override string ToString() //overrides string to display the items
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format("Item Num: {0},", this.ItemNum));
            sb.Append(string.Format(" Description: {0},", this.Description));
            sb.Append(string.Format(" Price: {0:c}", this.Price));
            return sb.ToString();
        }
    }
}
