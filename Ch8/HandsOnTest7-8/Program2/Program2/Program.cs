﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program2
{
    class Program
    {
        static void Main(string[] args)
        {
            double salary;
            PromptSalary(out salary);
            CalculateNetPay(salary, insurance : 100, tax : 0.3);
            ReadLine();
        }

        static void PromptSalary(out double userInput)
        {
            Write("Enter annual salary: ");
            userInput = Convert.ToDouble(ReadLine());
        }

        static void CalculateNetPay(double salary, int insurance = 100, double tax = 0.3)
        {
            WriteLine("Type 1 for gross pay, 2 for net pay w/ insurance and no tax, or 3 for net pay w/o insurance but w/ tax");
            WriteLine("Type 2 for net pay w/ insurance and no tax");
            WriteLine("Type 3 for net pay w/o insurance but w/ tax");

            double userInput;
            double netPay;
            double netPayTax;

            userInput = Convert.ToDouble(ReadLine());
            netPay = salary - insurance;
            netPayTax = salary - (salary * tax);
            if (userInput == 1)
            {
                WriteLine("The salary is: " + salary);
            }
            else if (userInput == 2)
            {
                WriteLine("The net pay w/ insurance is: " + netPay);
            }
            else if (userInput == 3)
            {
                WriteLine("The netpay after taxes is: " + netPayTax);
            }
        }
    }
}
