﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program1
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = new double[15];
            PromptNumbers(array);
            CalculateStatistics(array, out double highest, out double lowest, out double sum, out double avg);
            WriteLine("Highest: " + highest);
            WriteLine("Lowest: " + lowest);
            WriteLine("Sum: " + sum);
            WriteLine("Average: " + avg);
            ReadLine();
        }

        static void PromptNumbers(double[] integers)
        {
            double userInput;

            for (int i = 0; i < integers.Length; i++)
            {
                WriteLine("Enter a number " + (i + 1) + " or 999 to end");
                if (!Double.TryParse(ReadLine(), out userInput))
                {
                    WriteLine("Enter a valid number");
                    userInput = Convert.ToDouble(ReadLine());
                }
                if (userInput != 999)
                {
                    integers[i] = userInput;
                }
                else
                {
                    i = 16;
                }
            }
        }

        static void CalculateStatistics(double[] integers2, out double highest, out double lowest, out double sum, out double avg)
        {
            highest = Convert.ToDouble(integers2.Max());
            lowest = Convert.ToDouble(integers2.Min());
            sum = Convert.ToDouble(integers2.Sum());
            avg = Convert.ToDouble(integers2.Average());
        }
    }
}
