﻿using System;
using static System.Console;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program1
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = new double[15];
            int globalCounter = 0;
            PromptNumbers(array, globalCounter);
            CalculateStatistics(array, out double highest, out double lowest, out double sum, out double avg, globalCounter);
            WriteLine("Highest: " + highest);
            WriteLine("Lowest: " + lowest);
            WriteLine("Sum: " + sum);
            WriteLine("Average: " + avg);
            ReadLine();
        }

        static void PromptNumbers(double[] integers, int integerCounter)
        {
            double userInput;
            string stringInput;
            int counter = 0;

            for (int i = 0; i < integers.Length; i++)
            {
                WriteLine("Enter a number " + (i + 1) + " or Z to end");
                //if (!Double.TryParse(ReadLine(), out userInput))
                //{
                //    WriteLine("Enter a valid number");
                //    userInput = Convert.ToDouble(ReadLine());
                //}
                //if (userInput != 999)
                //{
                //    integers[i] = userInput;
                //}
                //else
                //{
                //    i = 16;
                //}
                stringInput = Convert.ToString(ReadLine()).ToLower();
                if (Double.TryParse(stringInput, out userInput))
                {
                    if (stringInput == "0")
                    {
                        integerCounter++;
                    }
                    integers[i] = userInput;
                }
                else
                {
                    if (stringInput == "z")
                    {
                        return;
                    }
                    else
                    {
                        WriteLine("enter a valid number");
                    }
                }

            }
        }

        static void CalculateStatistics(double[] integers2, out double highest, out double lowest, out double sum, out double avg, int globalCounter)
        {
            System.Data.DataTable table = new DataTable("ParentTable");
            // Declare variables for DataColumn and DataRow objects.
            DataColumn column;
            DataRow row;

            // Create new DataColumn, set DataType, 
            // ColumnName and add to DataTable.    
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "value";
            // Add the Column to the DataColumnCollection.
            table.Columns.Add(column);

            for (int i = 0; i < integers2.Length; i++)
            {
                if (integers2[i] != 0)
                {
                    row = table.NewRow();
                    row["value"] = integers2[i];
                    table.Rows.Add(row);
                }
            }
            for (int i = 0; i < globalCounter; i++)
            {

            }
            double[] integers3 = new double[table.Rows.Count];
            int counter = 0;

            foreach (DataRow dataRow in table.Rows)
            {
                Double conversion = Convert.ToDouble(dataRow["value"]);
                integers3[counter] = conversion;
                counter++;
            }


            highest = Convert.ToDouble(integers3.Max());
            lowest = Convert.ToDouble(integers3.Min());
            sum = Convert.ToDouble(integers3.Sum());
            avg = Convert.ToDouble(integers3.Average());
        }
    }
}
