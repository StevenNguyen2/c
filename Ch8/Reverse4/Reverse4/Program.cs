﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reverse4
{
    class Program
    {
        static void Main(string[] args)
        {
            int firstInt = 1;
            int secondInt = 2;
            int thirdInt = 3;
            int lastInt = 4;

            WriteLine("Original position: " + firstInt);
            WriteLine("Original position: " + secondInt);
            WriteLine("Original position: " + thirdInt);
            WriteLine("Original position: " + lastInt);
            WriteLine("After Reverse: ");
            Reverse(ref firstInt, ref secondInt, ref thirdInt, ref lastInt);
            WriteLine("After Reverse: " + firstInt);
            WriteLine("After Reverse: " + secondInt);
            WriteLine("After Reverse: " + thirdInt);
            WriteLine("After Reverse: " + lastInt);
            ReadLine();
        }

        public static void Reverse (ref int first, ref int second, ref int third, ref int last)
        {
            int holder = first;
            int holder2 = second;
            first = last;
            second = third;
            third = holder2;
            last = holder;
        }
    }
}
