﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace film
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            string movie = "Movie";
            int time = 200;

            Film(movie, time);
            Film(movie);
            Console.ReadKey();
        }

        public static void Film(string movie, int time = 90)
        {
            Console.WriteLine("Movie: " + movie);
            Console.WriteLine("Running Time: " + time);
        }

    }
}
