﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reverse3
{
    class Program
    {
        static void Main (string[] args)
        {
            int firstInt = 1;
            int middleInt = 2;
            int lastInt = 3;

            WriteLine("Original position: " + firstInt);
            WriteLine("Original position: " + middleInt);
            WriteLine("Original position: " + lastInt);
            WriteLine("After Reverse: ");
            Reverse(ref firstInt, ref middleInt, ref lastInt);
            WriteLine("After Reverse: " + firstInt);
            WriteLine("After Reverse: " + middleInt);
            WriteLine("After Reverse: " + lastInt);
            ReadLine();
        }

        public static void Reverse (ref int first, ref int middle, ref int last)
        {
            int holder = first;
            first = last;
            last = holder;
        }
    }
}
