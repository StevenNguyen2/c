﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace Averages
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("Display how many numbers?");

            int userInput = Convert.ToInt32(ReadLine());
            double[] array = new double[userInput];
            Averages(array);
            ReadLine();
        }

        static void Averages(double[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                WriteLine("Enter number " + (i + 1));
                array[i] = Convert.ToDouble(ReadLine());
            }
            double average = Convert.ToDouble(array.Average());

            WriteLine("The numbers in your array: ");
            for (int i = 0; i < array.Length; i++)
            {
                WriteLine(array[i].ToString());
            }

            WriteLine("Average is: " + average);
        }
    }
}