﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Animated
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            label1.Text = "Has been clicked";
        }

        private void Form1_MouseEnter(object sender, EventArgs e)
        {
            label1.Text = "Mouse is not over the picture box";
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            label1.Text = "Has been hovered over";
        }
    }
}
