﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleExample
{
    delegate void GreetingDelegate(string s); //can only point to methods w/ return type of 'void' and parameter of 'string'
    class Program
    {
        public static void Hello(string s)
        {
            WriteLine("Hello, {0}!", s);
        }

        public static void Goodbye(string s)
        {
            WriteLine("Goodbye, {0}!", s);
        }

        static void Main(string[] args)
        {
            GreetingDelegate firstDel, secondDel;
            firstDel = new GreetingDelegate(Hello);
            secondDel = new GreetingDelegate(Goodbye);
            firstDel += secondDel;
            GreetMethod(firstDel, "Cathy");
            GreetMethod(secondDel, "Bob");
            ReadLine();
        }

        public static void GreetMethod(GreetingDelegate gd, string name)
        {
            WriteLine("The greeting is:");
            gd(name);
        }
    }
}
