﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClassicBookSelector
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void listBox2_MouseEnter(object sender, EventArgs e)
        {
            label3.Text = "reading is great";
        }

        private void Form1_MouseEnter(object sender, EventArgs e)
        {
            label3.Text = "";
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox2.SelectedItem != null)
            {
                if (listBox2.SelectedItem.ToString() == "Bible")
                {
                    label4.Text = "This is great";
                    this.BackColor = System.Drawing.Color.Blue;
                }
                if (listBox2.SelectedItem.ToString() == "Quran")
                {
                    label5.Text = "This is also great";
                    this.BackColor = System.Drawing.Color.Red;

                }
                if (listBox2.SelectedItem.ToString() == "Torah")
                {
                    label6.Text = "This is even better";
                    this.BackColor = System.Drawing.Color.Yellow;
                }
                if (listBox2.SelectedItem.ToString() == "Harry Potter")
                {
                    label7.Text = "This is not good";
                    this.BackColor = System.Drawing.Color.Gold;
                }
                if (listBox2.SelectedItem.ToString() == "Dictionary")
                {
                    label8.Text = "This is terrible";
                    this.BackColor = System.Drawing.Color.Green;
                }
            }
        }
    }
}
