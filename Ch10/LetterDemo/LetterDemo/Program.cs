﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LetterDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Letter letter1 = new Letter("Steven", "10/14/17");
            CertifiedLetter letter2 = new CertifiedLetter(1234567);
            letter2.name = "STEVEN";
            letter2.dateMailed = "10/16/17";
            Console.WriteLine(letter1.ToString());
            Console.WriteLine(letter2.ToString());
            Console.ReadLine();
        }
    }
}