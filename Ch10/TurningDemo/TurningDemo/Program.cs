﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurningDemo
{
    public interface ITurnable
    {
        string Turn();
    }

    class Page : ITurnable
    {
        public string Turn()
        {
            return "You turn the page in the book.";
        }
    }

    class Corner : ITurnable
    {
        public string Turn()
        {
            return "You turn the corner at Ranken and Kingshighway.";
        }
    }

    class Pancake : ITurnable
    {
        public string Turn()
        {
            return "You turn the waffle over.";
        }
    }

    class Leaf : ITurnable
    {
        public string Turn()
        {
            return "You turn the phone over.";
        }
    }

    class DemoTurn
    {
        public static void Main()
        {
            Page one = new Page();
            Corner two = new Corner();
            Pancake three = new Pancake();
            Leaf four = new Leaf();

            Console.WriteLine("{0}", one.Turn());
            Console.WriteLine();
            Console.WriteLine("{0}", two.Turn());
            Console.WriteLine();
            Console.WriteLine("{0}", three.Turn());
            Console.WriteLine();
            Console.WriteLine("{0}", four.Turn());

            Console.ReadLine();
        }
    }
}