﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TurningDemo
{
    public interface IRecoverable
    {
        string Recover();
    }

    class Patient : IRecoverable
    {
        public string Recover()
        {
            return "I am getting better";
        }
    }

    class Upholsterer : IRecoverable
    {
        public string Recover()
        {
            return "You need to upholster my car seats.";
        }
    }

    class FootballPlayer : IRecoverable
    {
        public string Recover()
        {
            return "You are going to tackle him.";
        }
    }


    class DemoTurn
    {
        public static void Main()
        {
            Patient one = new Patient();
            Upholsterer two = new Upholsterer();
            FootballPlayer three = new FootballPlayer();

            Console.WriteLine("{0}", one.Recover());
            Console.WriteLine();
            Console.WriteLine("{0}", two.Recover());
            Console.WriteLine();
            Console.WriteLine("{0}", three.Recover());
            Console.ReadLine();
        }
    }
}