﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SumInts
{
    class Program
    {
        static void Main(string[] args)
        {
            int number1;
            int sum = 0;

            //prompt user to input number
            WriteLine("Enter a number: ");
            number1 = Convert.ToInt32(ReadLine());

            while (number1 != 999)
            {
                sum += number1;
                WriteLine("Please enter a number: ");
                number1 = Convert.ToInt32(ReadLine());
            }

            //display sum
            WriteLine("The sum of the numbers is {0}", sum);
            ReadLine();
        }
    }
}
