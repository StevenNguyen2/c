﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeSales
{
    class Program
    {
        static void Main(string[] args)
        {
            string salesPerson;
            double dTotal = 0.00;
            double eTotal = 0.00;
            double fTotal = 0.00;

            WriteLine("Enter an initial (D, E, or F)");
            salesPerson = ReadLine().ToString().ToLower();

            //Check if salesPerson value equals to D, E, F, or Z
            while (salesPerson == "d" || salesPerson == "e" || salesPerson == "f" || salesPerson == "z")
            {
                if (salesPerson == "d")
                {
                    WriteLine("Enter a sales total: ");
                    dTotal += Convert.ToDouble(ReadLine());
                    WriteLine("Enter an initial (D, E, or F): ");
                    salesPerson = ReadLine().ToString().ToLower();
                }
                else if (salesPerson == "e")
                {
                    WriteLine("Enter a sales total: ");
                    eTotal += Convert.ToDouble(ReadLine());
                    WriteLine("Enter an initial (D, E, or F): ");
                    salesPerson = ReadLine().ToString().ToLower();
                }
                else if (salesPerson == "f")
                {
                    WriteLine("Enter a sales total: ");
                    fTotal += Convert.ToDouble(ReadLine());
                    WriteLine("Enter an initial (D, E, or F): ");
                    salesPerson = ReadLine().ToString().ToLower();
                }
                else if (salesPerson == "z")
                {
                    //Display each saleperson's total
                    WriteLine("Danielle: " + dTotal.ToString("C"));
                    WriteLine("Edward: " + eTotal.ToString("C"));
                    WriteLine("Francis: " + fTotal.ToString("C"));

                    //Display a grand total for all sales
                    WriteLine("The grand total: " + (dTotal + eTotal + fTotal).ToString("C"));
                    salesPerson = "";

                    //Display the name of the salesperson w/ the highest total
                    if (dTotal > fTotal)
                    {
                        if (dTotal > eTotal)
                        {
                            //dTotal is the greatest then
                            WriteLine("Name of salesperson w/ highest total: Danielle");
                        }
                        else if (eTotal > fTotal)
                        {
                            //eTotal is greatest then
                            WriteLine("Name of salesperson w/ highest total: Edward");
                        }
                        else
                        {
                            //fTotal is the greatest then
                            WriteLine("Name of salesperson w/ highest total: Francis");
                        }
                    }
                    else if (eTotal > fTotal)
                    {
                        WriteLine("Name of salesperson w/ highest total: Edward");
                    }
                    else
                    {
                        WriteLine("Name of salesperson w/ highest total: Francis");
                    }
                }
            }
            if (salesPerson != "d" && salesPerson != "e" && salesPerson != "f" && salesPerson != "z" && salesPerson != "")
            {
                WriteLine("Error: Invalid Initial Value");
            }
            ReadLine();
        }
    }
}
