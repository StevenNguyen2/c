﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAddress
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("Enter a business name: ");
            string userInput = ReadLine();
            string noSpace = "";

            for (int i = 0; i < userInput.Length; i++)
            {
                //if (userInput.Substring(i, 1) == " ")
                if (userInput[i] != ' ')
                {
                    noSpace += userInput[i];
                }
            }

            WriteLine("www." + noSpace + ".com");
            ReadLine();
        }
    }
}
