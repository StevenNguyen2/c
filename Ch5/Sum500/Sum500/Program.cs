﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sum500
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum = 0;
            for (int i = 0; i <= 500; i++)
            {
                sum += i;
            }
            WriteLine("The sums of the integers from 1 through 500: " + sum);
            ReadLine();
        }
    }
}
