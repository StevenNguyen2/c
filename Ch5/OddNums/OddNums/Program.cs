﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddNums
{
    class Program
    {
        static void Main(string[] args)
        {
            int counter;

            for (counter = 1; counter <= 99; counter += 2)
            {
                WriteLine("Odd: " + counter);
            }
            ReadLine();
        }
    }
}
