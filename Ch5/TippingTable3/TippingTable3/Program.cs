﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TippingTable3
{
    class Program
    {
        static void Main(string[] args)
        {
            double dinnerPrice;
            double tipRate;
            double tip;
            double LOWRATE;
            double MAXRATE;
            const double TIPSTEP = 0.05;
            const double DINNERSTEP = 10.00;
            double MAXDINNER;

            WriteLine("Enter Lowest Tipping Percentage: ");
            LOWRATE = Convert.ToDouble(ReadLine());

            WriteLine("Enter Highest Tipping Percentage: ");
            MAXRATE = Convert.ToDouble(ReadLine());

            WriteLine("Enter Lowest Possible Restaurant Bill: ");
            dinnerPrice = Convert.ToDouble(ReadLine());

            WriteLine("Enter Highest Restaurant Bill: ");
            MAXDINNER = Convert.ToDouble(ReadLine());

            //More efficient
            Write("   Price");
            //Write("{0, 8}", "Price");
            for (tipRate = LOWRATE; tipRate <= MAXRATE; tipRate += TIPSTEP)
            {
                Write("{0, 8}", tipRate.ToString("F"));
            }
            WriteLine();
            WriteLine("--------------------------------------");

            tipRate = LOWRATE;

            while (dinnerPrice <= MAXDINNER)
            {
                Write("{0, 8}", dinnerPrice.ToString("C"));
                while (tipRate <= MAXRATE)
                {
                    tip = dinnerPrice * tipRate;
                    Write("{0, 8}", tip.ToString("F"));
                    tipRate += TIPSTEP;
                }
                dinnerPrice += DINNERSTEP;
                tipRate = LOWRATE;
                WriteLine();
            }
            ReadLine();
        }
    }
}
