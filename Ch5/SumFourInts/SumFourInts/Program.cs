﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SumFourInts
{
    class Program
    {
        static void Main(string[] args)
        {
            //Declare Variables
            int sum = 0;

            for (int i = 0; i < 4; i++)
            {
                WriteLine("Enter Integer " + (i + 1) + ":");
                sum += Convert.ToInt32(ReadLine());
            }

            //display sum
            WriteLine("Sum is: " + sum);

            ReadLine();
        }
    }
}
