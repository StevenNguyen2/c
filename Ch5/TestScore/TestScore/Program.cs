﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestScore
{
    class Program
    {
        static void Main(string[] args)
        {
            int numScores = 0;
            int total = 0;
            int userInput = 0;
            int sentValue = 999;

            while (userInput != sentValue)
            {
                WriteLine("Input number, and type 999 when done");
                userInput = Convert.ToInt32(ReadLine());
                if (userInput >= 0 && userInput <= 100)
                {
                    total += userInput;
                    numScores++;
                }
                else
                {
                    WriteLine("Error");
                }
            }
            WriteLine("The total is: " + total);
            WriteLine("The average is: " + (total / numScores));
            ReadLine();
        }
    }
}
