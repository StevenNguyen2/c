﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiplicationTable
{
    class Program
    {
        static void Main(string[] args)
        {
            int userInput = 1;

            WriteLine("Enter integer from 1 to 7");
            userInput = Convert.ToInt32(ReadLine());
            WriteLine("Multiplication Table:");
            WriteLine("____________________");
            for (int i = 1; i <= 7; i++)
            {
                for (int e = 1; e <= 10; e++)
                {
                    WriteLine(e + " X " + userInput + ": " + (userInput * e));
                    i++;
                }
            }
            ReadLine();
        }
    }
}
