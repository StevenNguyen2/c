﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnterLowercaseLetters
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("Type a lower case letter");
            while (true)
            {
                var inputChar = ReadKey().KeyChar;
                if (Char.IsLower(inputChar))
                {
                    WriteLine("");
                    WriteLine("Valid");
                }
                else
                {
                    WriteLine("");
                    WriteLine("Error: not a lowercase letter");
                }

                if (inputChar == '!')
                {
                    break;
                }
            }
        }
    }
}
