﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOT
{
    class BankLoan
    {
        private const double INTERESTRATE = .05;
        private double loanAmount;

        public double LoanAmount
        {
            //Property to change and set the loan amount
            get
            {
                return loanAmount;
            }
            set
            {
                loanAmount = value;
            }
        }

        public virtual double SubtractFromLoan(double _loanAmount)
        {
            //Method to make a payment that'll substract 200
            loanAmount = _loanAmount - 200;
            return loanAmount;
        }

        public double CalculateNewLoan(double _loanAmount)
        {
            //Method that calculates a new loan amount by adding interest to the loan amount
            loanAmount += (_loanAmount * INTERESTRATE);
            return loanAmount;
        }
    }
}
