﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOT
{
    class CarLoan : BankLoan
    {
        private string dealerName;

        public string DealerName
        {
            get
            {
                return dealerName;
            }
            set
            {
                dealerName = value;
            }
        }

        public override double SubtractFromLoan(double _loanAmount)
        {
            //return base.SubtractFromLoan(_loanAmount) - 450;
            return (_loanAmount - 450);
        }
    }
}
