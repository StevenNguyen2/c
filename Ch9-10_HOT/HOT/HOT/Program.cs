﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HOT
{
    class Program
    {
        static void Main(string[] args)
        {
            double userInput = 0;
            //double anotherInput;
            //double number;
            double dealerName;
            string _dealerName;
            BankLoan bankObject = new BankLoan();
            CarLoan carObject = new CarLoan();

            //do
            //{
            //    WriteLine("What was the total bank loan amount?");
            //    userInput = Convert.ToDouble(ReadLine());
            //    WriteLine("After 1 payment the car loan is down to: " + bankObject.SubtractFromLoan(userInput).ToString("C"));
            //    WriteLine("After 1 interest fee accumulation the loan is now: " + bankObject.CalculateNewLoan(bankObject.SubtractFromLoan(userInput)).ToString("C"));

            //    WriteLine("");

            //    CarLoan carObject = new CarLoan();
            //    //anotherInput = Convert.ToDouble(ReadLine());
            //    WriteLine("Who was the auto dealer for your car loan?");
            //    string dealerName = ReadLine();
            //    WriteLine("What was the total car loan amount?");
            //    anotherInput = Convert.ToDouble(ReadLine());
            //    WriteLine("The auto dealer was " + dealerName);
            //    WriteLine("After 1 payment the car loan is down to: " + carObject.SubtractFromLoan(anotherInput).ToString("C"));
            //    WriteLine("After 1 interest fee accumulation the loan is now: " + carObject.CalculateNewLoan(carObject.SubtractFromLoan(anotherInput)).ToString("C"));
            //}
            //while (!uint.TryParse(Convert.ToString(userInput), out number));

            while (userInput == 0)
            {
                WriteLine("What was the total bank loan amount?");
                Double.TryParse(ReadLine(), out userInput);
            }
            
            while (userInput != 0)
            {
                WriteLine("After 1 payment the car loan is down to " + bankObject.SubtractFromLoan(userInput).ToString("C"));
                WriteLine("After 1 interest fee accumulation the loan is now: " + bankObject.CalculateNewLoan(bankObject.SubtractFromLoan(userInput)).ToString("C"));
                break;
            }

            WriteLine("");
            WriteLine("Who was the auto dealer for your car loan?");
            _dealerName = ReadLine();
            Double.TryParse(_dealerName, out dealerName);

            while (dealerName != 0)
            {
                WriteLine("Enter a valid name");
                Double.TryParse(ReadLine(), out dealerName);
            }

            WriteLine("What was the total bank loan amount");
            Double.TryParse(ReadLine(), out userInput);
            while (userInput != 0)
            {
                WriteLine("The auto dealer was " + _dealerName);
                WriteLine("After 1 payment the car loan is down to " + carObject.SubtractFromLoan(userInput).ToString("C"));
                WriteLine("After 1 interest fee accumulation the loan is now " + carObject.CalculateNewLoan(carObject.SubtractFromLoan(userInput)).ToString("C"));
                break;
            }

            ReadLine();
        }
    }
}
