﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodOrder
{
    public class TooManyFlavors : ApplicationException
    {
        public TooManyFlavors() : base("Too many flavors")
        {

        }
    }
}
