﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodOrder
{
    public class TooManyToppings : ApplicationException
    {
        public TooManyToppings() : base("Too many toppings")
        {

        }
    }
}
