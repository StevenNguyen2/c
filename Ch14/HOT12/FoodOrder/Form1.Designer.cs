﻿namespace FoodOrder
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.sundaeCheckBox = new System.Windows.Forms.CheckBox();
            this.sodaCheckBox = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.sprinklesCheckBox = new System.Windows.Forms.CheckBox();
            this.chocolateCheckBox = new System.Windows.Forms.CheckBox();
            this.nutsCheckBox = new System.Windows.Forms.CheckBox();
            this.mangoCheckBox = new System.Windows.Forms.CheckBox();
            this.peachCheckBox = new System.Windows.Forms.CheckBox();
            this.limeCheckBox = new System.Windows.Forms.CheckBox();
            this.sodaErrorLabel = new System.Windows.Forms.Label();
            this.sundaeErrorLabel = new System.Windows.Forms.Label();
            this.addButton = new System.Windows.Forms.Button();
            this.orderTextBox = new System.Windows.Forms.TextBox();
            this.priceLabel = new System.Windows.Forms.Label();
            this.nameErrorLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Order";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(266, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "What name do you want on the order?";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(16, 68);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(270, 20);
            this.nameTextBox.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 127);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(168, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "Do you want a sundae?";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(190, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(152, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "Do you want a soda?";
            // 
            // sundaeCheckBox
            // 
            this.sundaeCheckBox.AutoSize = true;
            this.sundaeCheckBox.Location = new System.Drawing.Point(17, 147);
            this.sundaeCheckBox.Name = "sundaeCheckBox";
            this.sundaeCheckBox.Size = new System.Drawing.Size(44, 17);
            this.sundaeCheckBox.TabIndex = 6;
            this.sundaeCheckBox.Text = "Yes";
            this.sundaeCheckBox.UseVisualStyleBackColor = true;
            this.sundaeCheckBox.CheckedChanged += new System.EventHandler(this.sundaeCheckBox_CheckedChanged);
            // 
            // sodaCheckBox
            // 
            this.sodaCheckBox.AutoSize = true;
            this.sodaCheckBox.Location = new System.Drawing.Point(193, 147);
            this.sodaCheckBox.Name = "sodaCheckBox";
            this.sodaCheckBox.Size = new System.Drawing.Size(44, 17);
            this.sodaCheckBox.TabIndex = 7;
            this.sodaCheckBox.Text = "Yes";
            this.sodaCheckBox.UseVisualStyleBackColor = true;
            this.sodaCheckBox.CheckedChanged += new System.EventHandler(this.drinkCheckBox_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(19, 181);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(131, 16);
            this.label5.TabIndex = 8;
            this.label5.Text = "Sundae Toppings";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(190, 181);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 16);
            this.label6.TabIndex = 9;
            this.label6.Text = "Drink Mixins";
            // 
            // sprinklesCheckBox
            // 
            this.sprinklesCheckBox.AutoSize = true;
            this.sprinklesCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.sprinklesCheckBox.Location = new System.Drawing.Point(19, 197);
            this.sprinklesCheckBox.Name = "sprinklesCheckBox";
            this.sprinklesCheckBox.Size = new System.Drawing.Size(85, 21);
            this.sprinklesCheckBox.TabIndex = 10;
            this.sprinklesCheckBox.Text = "Sprinkles";
            this.sprinklesCheckBox.UseVisualStyleBackColor = true;
            // 
            // chocolateCheckBox
            // 
            this.chocolateCheckBox.AutoSize = true;
            this.chocolateCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.chocolateCheckBox.Location = new System.Drawing.Point(19, 240);
            this.chocolateCheckBox.Name = "chocolateCheckBox";
            this.chocolateCheckBox.Size = new System.Drawing.Size(131, 21);
            this.chocolateCheckBox.TabIndex = 11;
            this.chocolateCheckBox.Text = "Chocolate Syrup";
            this.chocolateCheckBox.UseVisualStyleBackColor = true;
            // 
            // nutsCheckBox
            // 
            this.nutsCheckBox.AutoSize = true;
            this.nutsCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.nutsCheckBox.Location = new System.Drawing.Point(19, 217);
            this.nutsCheckBox.Name = "nutsCheckBox";
            this.nutsCheckBox.Size = new System.Drawing.Size(56, 21);
            this.nutsCheckBox.TabIndex = 12;
            this.nutsCheckBox.Text = "Nuts";
            this.nutsCheckBox.UseVisualStyleBackColor = true;
            // 
            // mangoCheckBox
            // 
            this.mangoCheckBox.AutoSize = true;
            this.mangoCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.mangoCheckBox.Location = new System.Drawing.Point(193, 240);
            this.mangoCheckBox.Name = "mangoCheckBox";
            this.mangoCheckBox.Size = new System.Drawing.Size(113, 21);
            this.mangoCheckBox.TabIndex = 13;
            this.mangoCheckBox.Text = "Mango Flavor";
            this.mangoCheckBox.UseVisualStyleBackColor = true;
            // 
            // peachCheckBox
            // 
            this.peachCheckBox.AutoSize = true;
            this.peachCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.peachCheckBox.Location = new System.Drawing.Point(193, 217);
            this.peachCheckBox.Name = "peachCheckBox";
            this.peachCheckBox.Size = new System.Drawing.Size(110, 21);
            this.peachCheckBox.TabIndex = 14;
            this.peachCheckBox.Text = "Peach Flavor";
            this.peachCheckBox.UseVisualStyleBackColor = true;
            // 
            // limeCheckBox
            // 
            this.limeCheckBox.AutoSize = true;
            this.limeCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.limeCheckBox.Location = new System.Drawing.Point(193, 197);
            this.limeCheckBox.Name = "limeCheckBox";
            this.limeCheckBox.Size = new System.Drawing.Size(100, 21);
            this.limeCheckBox.TabIndex = 15;
            this.limeCheckBox.Text = "Lime Flavor";
            this.limeCheckBox.UseVisualStyleBackColor = true;
            // 
            // sodaErrorLabel
            // 
            this.sodaErrorLabel.AutoSize = true;
            this.sodaErrorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.sodaErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.sodaErrorLabel.Location = new System.Drawing.Point(190, 260);
            this.sodaErrorLabel.Name = "sodaErrorLabel";
            this.sodaErrorLabel.Size = new System.Drawing.Size(0, 17);
            this.sodaErrorLabel.TabIndex = 16;
            // 
            // sundaeErrorLabel
            // 
            this.sundaeErrorLabel.AutoSize = true;
            this.sundaeErrorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.sundaeErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.sundaeErrorLabel.Location = new System.Drawing.Point(19, 260);
            this.sundaeErrorLabel.Name = "sundaeErrorLabel";
            this.sundaeErrorLabel.Size = new System.Drawing.Size(0, 17);
            this.sundaeErrorLabel.TabIndex = 17;
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(16, 286);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(146, 24);
            this.addButton.TabIndex = 18;
            this.addButton.Text = "Add item to order";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // orderTextBox
            // 
            this.orderTextBox.Location = new System.Drawing.Point(364, 46);
            this.orderTextBox.Multiline = true;
            this.orderTextBox.Name = "orderTextBox";
            this.orderTextBox.Size = new System.Drawing.Size(199, 264);
            this.orderTextBox.TabIndex = 19;
            // 
            // priceLabel
            // 
            this.priceLabel.AutoSize = true;
            this.priceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.priceLabel.Location = new System.Drawing.Point(441, 330);
            this.priceLabel.Name = "priceLabel";
            this.priceLabel.Size = new System.Drawing.Size(54, 20);
            this.priceLabel.TabIndex = 20;
            this.priceLabel.Text = "Total:";
            // 
            // nameErrorLabel
            // 
            this.nameErrorLabel.AutoSize = true;
            this.nameErrorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.nameErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.nameErrorLabel.Location = new System.Drawing.Point(13, 91);
            this.nameErrorLabel.Name = "nameErrorLabel";
            this.nameErrorLabel.Size = new System.Drawing.Size(0, 17);
            this.nameErrorLabel.TabIndex = 21;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(575, 450);
            this.Controls.Add(this.nameErrorLabel);
            this.Controls.Add(this.priceLabel);
            this.Controls.Add(this.orderTextBox);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.sundaeErrorLabel);
            this.Controls.Add(this.sodaErrorLabel);
            this.Controls.Add(this.limeCheckBox);
            this.Controls.Add(this.peachCheckBox);
            this.Controls.Add(this.mangoCheckBox);
            this.Controls.Add(this.nutsCheckBox);
            this.Controls.Add(this.chocolateCheckBox);
            this.Controls.Add(this.sprinklesCheckBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.sodaCheckBox);
            this.Controls.Add(this.sundaeCheckBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox sundaeCheckBox;
        private System.Windows.Forms.CheckBox sodaCheckBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox sprinklesCheckBox;
        private System.Windows.Forms.CheckBox chocolateCheckBox;
        private System.Windows.Forms.CheckBox nutsCheckBox;
        private System.Windows.Forms.CheckBox mangoCheckBox;
        private System.Windows.Forms.CheckBox peachCheckBox;
        private System.Windows.Forms.CheckBox limeCheckBox;
        private System.Windows.Forms.Label sodaErrorLabel;
        private System.Windows.Forms.Label sundaeErrorLabel;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.TextBox orderTextBox;
        private System.Windows.Forms.Label priceLabel;
        private System.Windows.Forms.Label nameErrorLabel;
    }
}

