﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FoodOrder
{

    public partial class Form1 : Form
    {
        private double _totalPrice = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            Order order;

            string name = nameTextBox.Text;

            bool hasBurger = false, hasDrink = false;

            if (sundaeCheckBox.Checked)
            {
                hasBurger = true;
            }

            if (sodaCheckBox.Checked)
            {
                hasDrink = true;
            }

            try
            {
                order = new Order(name, hasBurger, hasDrink);
                nameErrorLabel.Text = "";
            }
            catch(NameMissing ex)
            {
                nameErrorLabel.Text = ex.Message;
                return;
            }
            catch(NoFood ex)
            {
                nameErrorLabel.Text = ex.Message;
                return;
            }
            try
            {
                if (sprinklesCheckBox.Checked)
                {
                    order.Sundae.AddTopping(SundaeTopping.SPRINKLES);
                }

                if (nutsCheckBox.Checked)
                {
                    order.Sundae.AddTopping(SundaeTopping.NUTS);
                }

                if (chocolateCheckBox.Checked)
                {
                    order.Sundae.AddTopping(SundaeTopping.CHOCOLATE_SYRUP);
                }
                sundaeErrorLabel.Text = "";
            }
            catch(TooManyToppings ex)
            {
                sundaeErrorLabel.Text = ex.Message;
                return;
            }
            try
            {
                if (limeCheckBox.Checked)
                {
                    order.Soda.AddFlavor(SodaFlavor.LIME);
                }

                if (peachCheckBox.Checked)
                {
                    order.Soda.AddFlavor(SodaFlavor.PEACH);
                }

                if (mangoCheckBox.Checked)
                {
                    order.Soda.AddFlavor(SodaFlavor.MANGO);
                }
            }
            catch(TooManyFlavors ex)
            {
                sodaErrorLabel.Text = ex.Message;

            }

            orderTextBox.Text += order.ToString();
            _totalPrice += order.Price;
            priceLabel.Text = String.Format("Total: {0:C}", _totalPrice);

            if (nameTextBox != null)
            {
                nameTextBox.Text = "";
            }
            if (sundaeCheckBox.Checked)
            {
                sundaeCheckBox.Checked = false;
            }
            if (sodaCheckBox.Checked)
            {
                sodaCheckBox.Checked = false;
            }
            if (limeCheckBox.Checked)
            {
                limeCheckBox.Checked = false;
            }
            if (peachCheckBox.Checked)
            {
                peachCheckBox.Checked = false;
            }
            if (mangoCheckBox.Checked)
            {
                mangoCheckBox.Checked = false;
            }
            if (sprinklesCheckBox.Checked)
            {
                sprinklesCheckBox.Checked = false;
            }
            if (nutsCheckBox.Checked)
            {
                nutsCheckBox.Checked = false;
            }
            if (chocolateCheckBox.Checked)
            {
                chocolateCheckBox.Checked = false;
            }
        }

        private void sundaeCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (sundaeCheckBox.Checked)
            {
                sprinklesCheckBox.Enabled = true;
                nutsCheckBox.Enabled = true;
                chocolateCheckBox.Enabled = true;

                sprinklesCheckBox.BackColor = Color.LightBlue;
                nutsCheckBox.BackColor = Color.LightBlue;
                chocolateCheckBox.BackColor = Color.LightBlue;
            }
            else
            {
                sprinklesCheckBox.Enabled = false;
                nutsCheckBox.Enabled = false;
                chocolateCheckBox.Enabled = false;

                sprinklesCheckBox.BackColor = Color.Transparent;
                nutsCheckBox.BackColor = Color.Transparent;
                chocolateCheckBox.BackColor = Color.Transparent;
            }
        }

        private void drinkCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (sodaCheckBox.Checked)
            {
                limeCheckBox.Enabled = true;
                peachCheckBox.Enabled = true;
                mangoCheckBox.Enabled = true;

                limeCheckBox.BackColor = Color.LightBlue;
                peachCheckBox.BackColor = Color.LightBlue;
                mangoCheckBox.BackColor = Color.LightBlue;
            }
            else
            {
                limeCheckBox.Enabled = false;
                peachCheckBox.Enabled = false;
                mangoCheckBox.Enabled = false;

                limeCheckBox.BackColor = Color.Transparent;
                peachCheckBox.BackColor = Color.Transparent;
                mangoCheckBox.BackColor = Color.Transparent;
            }
        }
    }
}
