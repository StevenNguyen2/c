﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileComparison
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("Comparing two files");
            FileInfo fileInfo1 = new FileInfo("C:\\Users\\Steven Nguyen\\Desktop\\Hello.txt");
            FileInfo fileInfo2 = new FileInfo("C:\\Users\\Steven Nguyen\\Desktop\\Hello2.rtf");

            double size1 = fileInfo1.Length;
            double size2 = fileInfo2.Length;

            WriteLine(size1);
            WriteLine(size2);
            ReadLine();
        }
    }
}
