﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace CustomizeAForm2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            const string DELIM = ",";
            const string FILENAME = "C:\\Users\\Steven Nguyen\\Desktop\\Settings.txt";
            string title;
            string color = "";
            int size = 999;

            FileStream outFile = new FileStream(FILENAME, FileMode.Create, FileAccess.Write);
            StreamWriter writer = new StreamWriter(outFile);

            title = textBox1.Text;

            if (radioButton1.Checked)
            {
                color = "Red";
            }
            else if (radioButton1.Checked)
            {
                color = "Blue";
            }
            else if (radioButton1.Checked)
            {
                color = "Yellow";
            }
            else if (radioButton1.Checked)
            {
                color = "Green";
            }

            if (checkedListBox1.SelectedItem == "400")
            {
                size = Convert.ToInt32(400);
            }
            else if (checkedListBox1.SelectedItem == "500")
            {
                size = Convert.ToInt32(500);
            }
            else if (checkedListBox1.SelectedItem == "600")
            {
                size = Convert.ToInt32(600);
            }
            else if (checkedListBox1.SelectedItem == "700")
            {
                size = Convert.ToInt32(700);
            }
            writer.WriteLine(color + DELIM + size + DELIM + title);
            writer.Close();
            outFile.Close();
            button1.Enabled = false;
        }
    }
}
