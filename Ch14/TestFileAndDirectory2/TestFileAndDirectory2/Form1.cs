﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestFileAndDirectory2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            DirectoryInfo directory = new DirectoryInfo("C:\\Users\\Steven Nguyen\\Desktop\\");
            FileInfo[] files = directory.GetFiles();
            for (int i = 0; i < files.Length; i++)
            {
                checkedListBox1.Items.Add(files[i].FullName);
            }

            checkedListBox1.SelectionMode = SelectionMode.One;
            checkedListBox1.CheckOnClick = true;
            label1.Text = string.Empty;
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string filePath = (string)checkedListBox1.SelectedItem;
            FileInfo file = new FileInfo(filePath);
            label1.Text = $"{filePath} was created on \n{file.CreationTime}";
        }
    }

    public class SelectedCheckedListItem
    {
        string SelectedItem { get; set; }
    }
}
