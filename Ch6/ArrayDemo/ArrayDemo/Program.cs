﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = { 1, 2, 3, 4, 5, 6, 7, 8 };
            int userInput = 0;

            WriteLine("Enter 1, 2, or 3. Enter 0 to quit");
            userInput = Convert.ToInt32(ReadLine());

            if (userInput == 1)
            {
                WriteLine("_____________");
                //View List From First to Last Position
                for (int x = 0; x <= 7; ++x)
                {
                    WriteLine(numbers[x]);
                }
            } else if (userInput == 2)
            {
                WriteLine("_____________");
                //View List From Last to First Position
                for (int x = 7; x >= 0; --x)
                {
                    WriteLine(numbers[x]);
                }
            } else if (userInput == 3)
            {
           
                WriteLine("_____________");
                //View Specific Position to View
                WriteLine("Choose a number from 1 to 8:");
                int number = (Convert.ToInt32(ReadLine()) - 1);
                WriteLine(numbers[number]);
            }
            ReadLine();
        }
    }
}
