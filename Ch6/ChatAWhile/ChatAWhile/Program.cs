﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatAWhile
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] areaCode = { 262, 414, 608, 715, 815, 920 };
            double[] perMinuteRate = { 0.07, 0.10, 0.05, 0.16, 0.24, 0.14 };
            int areaInput;
            double rateInput;

            WriteLine("Enter an area code:");
            areaInput = Convert.ToInt32(ReadLine());

            WriteLine("Enter length of call in minutes:");
            rateInput = Convert.ToDouble(ReadLine());

            for (int i = 0; i < areaCode.Length; i++)
            {
                if (areaCode[i] == areaInput)
                {
                    string costOfCall;
                    costOfCall = (perMinuteRate[i] * rateInput).ToString("C");
                    WriteLine("The total cost of the call: " + costOfCall);
                    break;
                }
                else if (i == 5)
                {
                    WriteLine("Invalid zip code");
                }
            }
            ReadLine();
        }
    }
}
