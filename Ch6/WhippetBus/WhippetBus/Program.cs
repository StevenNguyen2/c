﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhippetBus
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] ticketPrice = { 25.00, 40.00, 55.00, 70.00 };
            int[] miles = { 0, 100, 300, 500 };
            int userMiles;
            int counter;

            WriteLine("Enter a trip distance");
            userMiles = Convert.ToInt32(ReadLine());

            for (int i = 0; i < ticketPrice.Length; i++)
            {
                counter = i + 1;
                if (i == 3)
                {
                    if (userMiles >= miles[i])
                    {
                        WriteLine("The ticket price: " + ticketPrice[i]);
                        break;
                    }
                }
                if (userMiles >= miles[i] && userMiles < miles[counter])
                {
                    WriteLine("The ticket price: " + ticketPrice[i]);
                    break;
                }

            }
            ReadLine();
        }
    }
}
