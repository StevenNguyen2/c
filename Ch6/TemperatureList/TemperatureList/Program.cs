﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemperatureList
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] temps = { 10, 20, 30, 40, 50, 60, 70 };
            int sum = 0;
            int average;

            for (int i = 0; i < temps.Length; i++)
            {
                sum += temps[i];
            }

            average = (sum / temps.Length);

            for (int i = 0; i < temps.Length; i++)
            {
                WriteLine("Temperature " + (i + 1) + ": " + (average - temps[i]) + " is far off from the average");
            }
            ReadLine();
        }
    }
}
