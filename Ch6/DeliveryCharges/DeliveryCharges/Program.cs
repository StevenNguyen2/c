﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryCharges
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] zipCodes = { 11111, 22222, 33333, 44444, 55555, 66666, 77777, 88888, 99999, 10101 };
            int[] priceFee = { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 };
            int userZip;

            WriteLine("Enter a zip code");
            userZip = Convert.ToInt32(ReadLine());

            for (int i = 0; i < zipCodes.Length; i++)
            {
                if (zipCodes[i] == userZip)
                {
                    WriteLine("The price of delivery for the zip code: " + priceFee[i]);
                }
                else if (i == 9)
                {
                    WriteLine("Company does not deliver to this zip code");
                }
            }
            ReadLine();
        }
    }
}
