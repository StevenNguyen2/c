﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoresComparison
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] scores = new int[4];
            int[] HighToLow = new int[4];
            int[] LowToHigh = new int[4];
            int countSort = 0;
            int countReverse = 0;

            int userScore;

            //Puts User Inputs into Array
            for (int i = 0; i < scores.Length; i++)
            {
                WriteLine("Enter a test score between 1 to 100");
                WriteLine((i + 1) + ".");
                userScore = Convert.ToInt32(ReadLine());
                if (userScore > 100 || userScore < 0)
                {
                    WriteLine("Incorrect Input: Try again");
                }
                else
                {
                    scores[i] = userScore;
                }
            }

            //Array for High to Low/Low to High
            for (int i = 0; i < HighToLow.Length; i++)
            {
                HighToLow[i] = scores[i];
                LowToHigh[i] = scores[i];
            }

            Array.Sort(LowToHigh);
            Array.Sort(HighToLow);
            Array.Reverse(HighToLow);
            
            //Array to sort LowToHigh/HighToLow in correct order
            for (int i = 0; i < scores.Length; i++)
            {
                if (scores[i] == LowToHigh[i])
                {
                    ++countSort;
                }
                if (scores[i] == HighToLow[i])
                {
                    ++countReverse;
                }
            }
            if (countSort == scores.Length)
            {
                WriteLine("Great Improvement");
                for (int i = 0; i < scores.Length; i++)
                {
                    Write("{0,5}", scores[i]);
                }
            }
            else if (countReverse == scores.Length)
            {
                WriteLine("Needs Improvement");
                for (int i = 0; i < scores.Length; i++)
                {
                    Write("{0,5}", scores[i]);
                }
            }
            else
            {
                WriteLine("Inconsistent Scores");
                for (int i = 0; i < scores.Length; i++)
                {
                    Write("{0,5}", scores[i]);
                }
            }
            ReadLine();
        }
    }
}
