﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckZips
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] zipCodes = { 11111, 22222, 33333, 44444, 55555, 66666, 77777, 88888, 99999, 10101 };
            int userInput;

            WriteLine("Please enter a zip code");
            userInput = Convert.ToInt32(ReadLine());

            for (int i = 0; i < zipCodes.Length; i++)
            {
                if (zipCodes[i] == userInput)
                {
                    WriteLine("Company delivers to the zip code");
                    break;
                }
                else if (i == 9)
                {
                    WriteLine("Company does not deliver to this zip code");
                }
            }
            ReadLine();
        }
    }
}
