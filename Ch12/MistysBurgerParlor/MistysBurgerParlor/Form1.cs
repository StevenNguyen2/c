﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MistysBurgerParlor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void cBoxBurger_CheckedChanged(object sender, EventArgs e)
        {
            cBoxOnion.BackColor = Color.Yellow;
            cBoxPickles.BackColor = Color.Yellow;
            cBoxCheese.BackColor = Color.Yellow;
        }

        private void cBoxDrink_CheckedChanged(object sender, EventArgs e)
        {
            cBoxCherry.BackColor = Color.Yellow;
            cBoxVanilla.BackColor = Color.Yellow;
            cBoxOrange.BackColor = Color.Yellow;
        }

        private void btnAddItem_Click(object sender, EventArgs e)
        {
            Order newOrder = new Order(lblName.Text, cBoxBurger.Checked, cBoxDrink.Checked);

        }
    }
}
