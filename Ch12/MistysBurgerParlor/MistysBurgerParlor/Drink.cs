﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MistysBurgerParlor
{
    class Drink
    {
        public const double BASE_PRICE = 1.50;
        public const double FLAVOR_PRICE = 0.10;
        private double _price;

        public double Price { get; }
    }
}
