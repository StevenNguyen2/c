﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MistysBurgerParlor
{
    class ApplicationException : Exception
    {
        public ApplicationException(string msg) : base(msg)
        {
            
        }

        
    }
}
