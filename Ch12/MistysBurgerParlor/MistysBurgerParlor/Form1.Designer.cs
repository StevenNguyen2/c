﻿namespace MistysBurgerParlor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblName = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.lblNameRequirement = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cBoxDrink = new System.Windows.Forms.CheckBox();
            this.cBoxBurger = new System.Windows.Forms.CheckBox();
            this.lblToppings = new System.Windows.Forms.Label();
            this.lblDrinkMixin = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cBoxCheese = new System.Windows.Forms.CheckBox();
            this.cBoxPickles = new System.Windows.Forms.CheckBox();
            this.cBoxOnion = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cBoxOrange = new System.Windows.Forms.CheckBox();
            this.cBoxVanilla = new System.Windows.Forms.CheckBox();
            this.cBoxCherry = new System.Windows.Forms.CheckBox();
            this.lblToppingsAllowed = new System.Windows.Forms.Label();
            this.lblMixinAllowed = new System.Windows.Forms.Label();
            this.btnAddItem = new System.Windows.Forms.Button();
            this.lblOrder = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblTotal = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(12, 22);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(278, 19);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "What name do you want on the order?";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(16, 45);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(276, 20);
            this.tbName.TabIndex = 1;
            // 
            // lblNameRequirement
            // 
            this.lblNameRequirement.AutoSize = true;
            this.lblNameRequirement.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameRequirement.Location = new System.Drawing.Point(16, 72);
            this.lblNameRequirement.Name = "lblNameRequirement";
            this.lblNameRequirement.Size = new System.Drawing.Size(109, 19);
            this.lblNameRequirement.TabIndex = 2;
            this.lblNameRequirement.Text = "Name Required";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 113);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 19);
            this.label1.TabIndex = 3;
            this.label1.Text = "Do you want a burger?";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(228, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(156, 19);
            this.label2.TabIndex = 4;
            this.label2.Text = "Do you want a drink?";
            // 
            // cBoxDrink
            // 
            this.cBoxDrink.AutoSize = true;
            this.cBoxDrink.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxDrink.Location = new System.Drawing.Point(232, 137);
            this.cBoxDrink.Name = "cBoxDrink";
            this.cBoxDrink.Size = new System.Drawing.Size(50, 23);
            this.cBoxDrink.TabIndex = 6;
            this.cBoxDrink.Text = "Yes";
            this.cBoxDrink.UseVisualStyleBackColor = true;
            this.cBoxDrink.CheckedChanged += new System.EventHandler(this.cBoxDrink_CheckedChanged);
            // 
            // cBoxBurger
            // 
            this.cBoxBurger.AutoSize = true;
            this.cBoxBurger.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxBurger.Location = new System.Drawing.Point(20, 137);
            this.cBoxBurger.Name = "cBoxBurger";
            this.cBoxBurger.Size = new System.Drawing.Size(50, 23);
            this.cBoxBurger.TabIndex = 7;
            this.cBoxBurger.Text = "Yes";
            this.cBoxBurger.UseVisualStyleBackColor = true;
            this.cBoxBurger.CheckedChanged += new System.EventHandler(this.cBoxBurger_CheckedChanged);
            // 
            // lblToppings
            // 
            this.lblToppings.AutoSize = true;
            this.lblToppings.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToppings.Location = new System.Drawing.Point(16, 187);
            this.lblToppings.Name = "lblToppings";
            this.lblToppings.Size = new System.Drawing.Size(120, 19);
            this.lblToppings.TabIndex = 8;
            this.lblToppings.Text = "Burger Toppings";
            // 
            // lblDrinkMixin
            // 
            this.lblDrinkMixin.AutoSize = true;
            this.lblDrinkMixin.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDrinkMixin.Location = new System.Drawing.Point(206, 187);
            this.lblDrinkMixin.Name = "lblDrinkMixin";
            this.lblDrinkMixin.Size = new System.Drawing.Size(94, 19);
            this.lblDrinkMixin.TabIndex = 9;
            this.lblDrinkMixin.Text = "Drink Mixins";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cBoxCheese);
            this.groupBox1.Controls.Add(this.cBoxPickles);
            this.groupBox1.Controls.Add(this.cBoxOnion);
            this.groupBox1.Location = new System.Drawing.Point(20, 211);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(122, 99);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // cBoxCheese
            // 
            this.cBoxCheese.AutoSize = true;
            this.cBoxCheese.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxCheese.Location = new System.Drawing.Point(7, 72);
            this.cBoxCheese.Name = "cBoxCheese";
            this.cBoxCheese.Size = new System.Drawing.Size(64, 19);
            this.cBoxCheese.TabIndex = 11;
            this.cBoxCheese.Text = "Cheese";
            this.cBoxCheese.UseVisualStyleBackColor = true;
            // 
            // cBoxPickles
            // 
            this.cBoxPickles.AutoSize = true;
            this.cBoxPickles.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxPickles.Location = new System.Drawing.Point(7, 47);
            this.cBoxPickles.Name = "cBoxPickles";
            this.cBoxPickles.Size = new System.Drawing.Size(65, 19);
            this.cBoxPickles.TabIndex = 11;
            this.cBoxPickles.Text = "Pickles";
            this.cBoxPickles.UseVisualStyleBackColor = true;
            // 
            // cBoxOnion
            // 
            this.cBoxOnion.AutoSize = true;
            this.cBoxOnion.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxOnion.Location = new System.Drawing.Point(7, 20);
            this.cBoxOnion.Name = "cBoxOnion";
            this.cBoxOnion.Size = new System.Drawing.Size(66, 19);
            this.cBoxOnion.TabIndex = 0;
            this.cBoxOnion.Text = "Onions";
            this.cBoxOnion.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cBoxOrange);
            this.groupBox2.Controls.Add(this.cBoxVanilla);
            this.groupBox2.Controls.Add(this.cBoxCherry);
            this.groupBox2.Location = new System.Drawing.Point(210, 211);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(122, 99);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            // 
            // cBoxOrange
            // 
            this.cBoxOrange.AutoSize = true;
            this.cBoxOrange.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxOrange.Location = new System.Drawing.Point(7, 72);
            this.cBoxOrange.Name = "cBoxOrange";
            this.cBoxOrange.Size = new System.Drawing.Size(104, 19);
            this.cBoxOrange.TabIndex = 11;
            this.cBoxOrange.Text = "Orange Flavor";
            this.cBoxOrange.UseVisualStyleBackColor = true;
            // 
            // cBoxVanilla
            // 
            this.cBoxVanilla.AutoSize = true;
            this.cBoxVanilla.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxVanilla.Location = new System.Drawing.Point(7, 47);
            this.cBoxVanilla.Name = "cBoxVanilla";
            this.cBoxVanilla.Size = new System.Drawing.Size(103, 19);
            this.cBoxVanilla.TabIndex = 11;
            this.cBoxVanilla.Text = "Vanilla Flavor";
            this.cBoxVanilla.UseVisualStyleBackColor = true;
            // 
            // cBoxCherry
            // 
            this.cBoxCherry.AutoSize = true;
            this.cBoxCherry.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cBoxCherry.Location = new System.Drawing.Point(7, 20);
            this.cBoxCherry.Name = "cBoxCherry";
            this.cBoxCherry.Size = new System.Drawing.Size(100, 19);
            this.cBoxCherry.TabIndex = 0;
            this.cBoxCherry.Text = "Cherry Flavor";
            this.cBoxCherry.UseVisualStyleBackColor = true;
            // 
            // lblToppingsAllowed
            // 
            this.lblToppingsAllowed.AutoSize = true;
            this.lblToppingsAllowed.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToppingsAllowed.Location = new System.Drawing.Point(20, 317);
            this.lblToppingsAllowed.Name = "lblToppingsAllowed";
            this.lblToppingsAllowed.Size = new System.Drawing.Size(139, 19);
            this.lblToppingsAllowed.TabIndex = 12;
            this.lblToppingsAllowed.Text = "2 toppings allowed";
            // 
            // lblMixinAllowed
            // 
            this.lblMixinAllowed.AutoSize = true;
            this.lblMixinAllowed.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMixinAllowed.Location = new System.Drawing.Point(206, 317);
            this.lblMixinAllowed.Name = "lblMixinAllowed";
            this.lblMixinAllowed.Size = new System.Drawing.Size(116, 19);
            this.lblMixinAllowed.TabIndex = 13;
            this.lblMixinAllowed.Text = "1 mixin allowed";
            // 
            // btnAddItem
            // 
            this.btnAddItem.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddItem.Location = new System.Drawing.Point(24, 355);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(146, 28);
            this.btnAddItem.TabIndex = 14;
            this.btnAddItem.Text = "Add item to order";
            this.btnAddItem.UseVisualStyleBackColor = true;
            this.btnAddItem.Click += new System.EventHandler(this.btnAddItem_Click);
            // 
            // lblOrder
            // 
            this.lblOrder.AutoSize = true;
            this.lblOrder.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrder.Location = new System.Drawing.Point(844, 22);
            this.lblOrder.Name = "lblOrder";
            this.lblOrder.Size = new System.Drawing.Size(49, 19);
            this.lblOrder.TabIndex = 15;
            this.lblOrder.Text = "Order";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(504, 60);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(392, 192);
            this.textBox1.TabIndex = 16;
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(804, 259);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(47, 19);
            this.lblTotal.TabIndex = 17;
            this.lblTotal.Text = "Total:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(908, 391);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lblOrder);
            this.Controls.Add(this.btnAddItem);
            this.Controls.Add(this.lblMixinAllowed);
            this.Controls.Add(this.lblToppingsAllowed);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblDrinkMixin);
            this.Controls.Add(this.lblToppings);
            this.Controls.Add(this.cBoxBurger);
            this.Controls.Add(this.cBoxDrink);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblNameRequirement);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.lblName);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label lblNameRequirement;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox cBoxDrink;
        private System.Windows.Forms.CheckBox cBoxBurger;
        private System.Windows.Forms.Label lblToppings;
        private System.Windows.Forms.Label lblDrinkMixin;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cBoxCheese;
        private System.Windows.Forms.CheckBox cBoxPickles;
        private System.Windows.Forms.CheckBox cBoxOnion;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox cBoxOrange;
        private System.Windows.Forms.CheckBox cBoxVanilla;
        private System.Windows.Forms.CheckBox cBoxCherry;
        private System.Windows.Forms.Label lblToppingsAllowed;
        private System.Windows.Forms.Label lblMixinAllowed;
        private System.Windows.Forms.Button btnAddItem;
        private System.Windows.Forms.Label lblOrder;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lblTotal;
    }
}

