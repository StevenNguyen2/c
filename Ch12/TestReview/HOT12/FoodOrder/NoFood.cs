﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodOrder
{
    public class NoFood : ApplicationException
    {
        public NoFood() : base("You must select food")
        {

        }
    }
}
