﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodOrder
{
    public class Order
    {
        private string _name;
        private Burger _burger;
        private Drink _drink;

        public string Name { get { return _name; } }
        public Burger Burger { get { return _burger; } }
        public Drink Drink { get { return _drink; } }
        public double Price
        {
            get
            {
                double total = 0;

                if(_burger != null)
                {
                    total += _burger.Price;
                }

                if(_drink != null)
                {
                    total += _drink.Price;
                }

                return total;
            }
        }

        public Order(string name, bool burger, bool drink)
        {
            _name = name;

            if (name == "")
            {
                throw new NameMissing();
            }

            if (burger == false && drink == false)
            {
                throw new NoFood();
            }

            if (burger)
            {
                _burger = new Burger();
            }

            if (drink)
            {
                _drink = new Drink();
            }
        }

        public override string ToString()
        {
            string order = _name + "\r\n-------------------\r\n";

            if(_burger != null)
            {
                order += _burger.ToString() + "\r\n";
            }
            if(_drink != null)
            {
                order += _drink.ToString() + "r\n";
            }

            return order;
        }
    }
}
