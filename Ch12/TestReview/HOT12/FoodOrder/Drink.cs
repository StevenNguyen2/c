﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodOrder
{
    public class Drink
    {
        public const double BASE_PRICE = 1.5;
        public const double FLAVOR_PRICE = 0.1;

        //private DrinkFlavor[] _flavorsCount = new DrinkFlavor[1];
        private DrinkFlavor _flavor;
        private double _price;

        public DrinkFlavor Flavor { get { return _flavor; } } 
        public double Price { get { return _price; } }

        public Drink()
        {
            _price += BASE_PRICE;
        }

        public void AddFlavor(DrinkFlavor f)
        {
            if(_flavor == DrinkFlavor.NONE)
            {
                _flavor = f;
                _price += FLAVOR_PRICE;
            }
            else
            {
                throw new TooManyFlavors();
            }
        }

        public override string ToString()
        {
            return String.Format("Drink - {0} - {1:C}", _flavor, _price);
        }
    }
}
