﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FoodOrder
{

    public partial class Form1 : Form
    {
        private double _totalPrice = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            Order order;

            string name = nameTextBox.Text;

            bool hasBurger = false, hasDrink = false;

            if (burgerCheckBox.Checked)
            {
                hasBurger = true;
            }

            if (drinkCheckBox.Checked)
            {
                hasDrink = true;
            }

            try
            {
                order = new Order(name, hasBurger, hasDrink);
                nameErrorLabel.Text = "";
            }
            catch(NameMissing ex)
            {
                nameErrorLabel.Text = ex.Message;
                return;
            }
            catch(NoFood ex)
            {
                nameErrorLabel.Text = ex.Message;
                return;
            }
            try
            {
                if (onionsCheckBox.Checked)
                {
                    order.Burger.AddTopping(BurgerToppings.ONIONS);
                }

                if (picklesCheckBox.Checked)
                {
                    order.Burger.AddTopping(BurgerToppings.PICKLES);
                }

                if (cheeseCheckBox.Checked)
                {
                    order.Burger.AddTopping(BurgerToppings.CHEESE);
                }
                burgerErrorLabel.Text = "";
            }
            catch(TooManyToppings ex)
            {
                burgerErrorLabel.Text = ex.Message;
                return;
            }
            try
            {
                if (cherryCheckBox.Checked)
                {
                    order.Drink.AddFlavor(DrinkFlavor.CHERRY);
                }

                if (vanillaCheckBox.Checked)
                {
                    order.Drink.AddFlavor(DrinkFlavor.VANILLA);
                }

                if (orangeCheckBox.Checked)
                {
                    order.Drink.AddFlavor(DrinkFlavor.ORANGE);
                }
            }
            catch(TooManyFlavors ex)
            {
                drinkErrorLabel.Text = ex.Message;

            }

            orderTextBox.Text += order.ToString();
            _totalPrice += order.Price;
            priceLabel.Text = String.Format("Total: {0:C}", _totalPrice);
        }

        private void burgerCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (burgerCheckBox.Checked)
            {
                onionsCheckBox.Enabled = true;
                picklesCheckBox.Enabled = true;
                cheeseCheckBox.Enabled = true;

                onionsCheckBox.BackColor = Color.Yellow;
                picklesCheckBox.BackColor = Color.Yellow;
                cheeseCheckBox.BackColor = Color.Yellow;
            }
            else
            {
                onionsCheckBox.Enabled = false;
                picklesCheckBox.Enabled = false;
                cheeseCheckBox.Enabled = false;

                onionsCheckBox.BackColor = Color.Transparent;
                picklesCheckBox.BackColor = Color.Transparent;
                cheeseCheckBox.BackColor = Color.Transparent;
            }
        }

        private void drinkCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (drinkCheckBox.Checked)
            {
                cherryCheckBox.Enabled = true;
                vanillaCheckBox.Enabled = true;
                orangeCheckBox.Enabled = true;

                cherryCheckBox.BackColor = Color.Yellow;
                vanillaCheckBox.BackColor = Color.Yellow;
                orangeCheckBox.BackColor = Color.Yellow;
            }
            else
            {
                cherryCheckBox.Enabled = false;
                vanillaCheckBox.Enabled = false;
                orangeCheckBox.Enabled = false;

                cherryCheckBox.BackColor = Color.Transparent;
                vanillaCheckBox.BackColor = Color.Transparent;
                orangeCheckBox.BackColor = Color.Transparent;
            }
        }
    }
}
