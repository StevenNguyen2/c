﻿namespace FoodOrder
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.burgerCheckBox = new System.Windows.Forms.CheckBox();
            this.drinkCheckBox = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.onionsCheckBox = new System.Windows.Forms.CheckBox();
            this.cheeseCheckBox = new System.Windows.Forms.CheckBox();
            this.picklesCheckBox = new System.Windows.Forms.CheckBox();
            this.orangeCheckBox = new System.Windows.Forms.CheckBox();
            this.vanillaCheckBox = new System.Windows.Forms.CheckBox();
            this.cherryCheckBox = new System.Windows.Forms.CheckBox();
            this.drinkErrorLabel = new System.Windows.Forms.Label();
            this.burgerErrorLabel = new System.Windows.Forms.Label();
            this.addButton = new System.Windows.Forms.Button();
            this.orderTextBox = new System.Windows.Forms.TextBox();
            this.priceLabel = new System.Windows.Forms.Label();
            this.nameErrorLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Order";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(189, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "What name do you want on the order?";
            // 
            // nameTextBox
            // 
            this.nameTextBox.Location = new System.Drawing.Point(13, 64);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(100, 20);
            this.nameTextBox.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Do you want a burger?";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(190, 110);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Do you want a drink?";
            // 
            // burgerCheckBox
            // 
            this.burgerCheckBox.AutoSize = true;
            this.burgerCheckBox.Location = new System.Drawing.Point(19, 127);
            this.burgerCheckBox.Name = "burgerCheckBox";
            this.burgerCheckBox.Size = new System.Drawing.Size(44, 17);
            this.burgerCheckBox.TabIndex = 6;
            this.burgerCheckBox.Text = "Yes";
            this.burgerCheckBox.UseVisualStyleBackColor = true;
            this.burgerCheckBox.CheckedChanged += new System.EventHandler(this.burgerCheckBox_CheckedChanged);
            // 
            // drinkCheckBox
            // 
            this.drinkCheckBox.AutoSize = true;
            this.drinkCheckBox.Location = new System.Drawing.Point(193, 127);
            this.drinkCheckBox.Name = "drinkCheckBox";
            this.drinkCheckBox.Size = new System.Drawing.Size(44, 17);
            this.drinkCheckBox.TabIndex = 7;
            this.drinkCheckBox.Text = "Yes";
            this.drinkCheckBox.UseVisualStyleBackColor = true;
            this.drinkCheckBox.CheckedChanged += new System.EventHandler(this.drinkCheckBox_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 181);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Burger Toppings";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(190, 181);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Drink Mixins";
            // 
            // onionsCheckBox
            // 
            this.onionsCheckBox.AutoSize = true;
            this.onionsCheckBox.Location = new System.Drawing.Point(19, 197);
            this.onionsCheckBox.Name = "onionsCheckBox";
            this.onionsCheckBox.Size = new System.Drawing.Size(59, 17);
            this.onionsCheckBox.TabIndex = 10;
            this.onionsCheckBox.Text = "Onions";
            this.onionsCheckBox.UseVisualStyleBackColor = true;
            // 
            // cheeseCheckBox
            // 
            this.cheeseCheckBox.AutoSize = true;
            this.cheeseCheckBox.Location = new System.Drawing.Point(19, 240);
            this.cheeseCheckBox.Name = "cheeseCheckBox";
            this.cheeseCheckBox.Size = new System.Drawing.Size(62, 17);
            this.cheeseCheckBox.TabIndex = 11;
            this.cheeseCheckBox.Text = "Cheese";
            this.cheeseCheckBox.UseVisualStyleBackColor = true;
            // 
            // picklesCheckBox
            // 
            this.picklesCheckBox.AutoSize = true;
            this.picklesCheckBox.Location = new System.Drawing.Point(19, 217);
            this.picklesCheckBox.Name = "picklesCheckBox";
            this.picklesCheckBox.Size = new System.Drawing.Size(60, 17);
            this.picklesCheckBox.TabIndex = 12;
            this.picklesCheckBox.Text = "Pickles";
            this.picklesCheckBox.UseVisualStyleBackColor = true;
            // 
            // orangeCheckBox
            // 
            this.orangeCheckBox.AutoSize = true;
            this.orangeCheckBox.Location = new System.Drawing.Point(193, 240);
            this.orangeCheckBox.Name = "orangeCheckBox";
            this.orangeCheckBox.Size = new System.Drawing.Size(93, 17);
            this.orangeCheckBox.TabIndex = 13;
            this.orangeCheckBox.Text = "Orange Flavor";
            this.orangeCheckBox.UseVisualStyleBackColor = true;
            // 
            // vanillaCheckBox
            // 
            this.vanillaCheckBox.AutoSize = true;
            this.vanillaCheckBox.Location = new System.Drawing.Point(193, 217);
            this.vanillaCheckBox.Name = "vanillaCheckBox";
            this.vanillaCheckBox.Size = new System.Drawing.Size(89, 17);
            this.vanillaCheckBox.TabIndex = 14;
            this.vanillaCheckBox.Text = "Vanilla Flavor";
            this.vanillaCheckBox.UseVisualStyleBackColor = true;
            // 
            // cherryCheckBox
            // 
            this.cherryCheckBox.AutoSize = true;
            this.cherryCheckBox.Location = new System.Drawing.Point(193, 197);
            this.cherryCheckBox.Name = "cherryCheckBox";
            this.cherryCheckBox.Size = new System.Drawing.Size(88, 17);
            this.cherryCheckBox.TabIndex = 15;
            this.cherryCheckBox.Text = "Cherry Flavor";
            this.cherryCheckBox.UseVisualStyleBackColor = true;
            // 
            // drinkErrorLabel
            // 
            this.drinkErrorLabel.AutoSize = true;
            this.drinkErrorLabel.Location = new System.Drawing.Point(190, 260);
            this.drinkErrorLabel.Name = "drinkErrorLabel";
            this.drinkErrorLabel.Size = new System.Drawing.Size(79, 13);
            this.drinkErrorLabel.TabIndex = 16;
            this.drinkErrorLabel.Text = "drink error label";
            // 
            // burgerErrorLabel
            // 
            this.burgerErrorLabel.AutoSize = true;
            this.burgerErrorLabel.Location = new System.Drawing.Point(19, 260);
            this.burgerErrorLabel.Name = "burgerErrorLabel";
            this.burgerErrorLabel.Size = new System.Drawing.Size(86, 13);
            this.burgerErrorLabel.TabIndex = 17;
            this.burgerErrorLabel.Text = "burger error label";
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(19, 305);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 18;
            this.addButton.Text = "Add item to order";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // orderTextBox
            // 
            this.orderTextBox.Location = new System.Drawing.Point(355, 47);
            this.orderTextBox.Multiline = true;
            this.orderTextBox.Name = "orderTextBox";
            this.orderTextBox.Size = new System.Drawing.Size(181, 264);
            this.orderTextBox.TabIndex = 19;
            // 
            // priceLabel
            // 
            this.priceLabel.AutoSize = true;
            this.priceLabel.Location = new System.Drawing.Point(500, 318);
            this.priceLabel.Name = "priceLabel";
            this.priceLabel.Size = new System.Drawing.Size(35, 13);
            this.priceLabel.TabIndex = 20;
            this.priceLabel.Text = "label7";
            // 
            // nameErrorLabel
            // 
            this.nameErrorLabel.AutoSize = true;
            this.nameErrorLabel.Location = new System.Drawing.Point(13, 91);
            this.nameErrorLabel.Name = "nameErrorLabel";
            this.nameErrorLabel.Size = new System.Drawing.Size(35, 13);
            this.nameErrorLabel.TabIndex = 21;
            this.nameErrorLabel.Text = "label7";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(575, 450);
            this.Controls.Add(this.nameErrorLabel);
            this.Controls.Add(this.priceLabel);
            this.Controls.Add(this.orderTextBox);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.burgerErrorLabel);
            this.Controls.Add(this.drinkErrorLabel);
            this.Controls.Add(this.cherryCheckBox);
            this.Controls.Add(this.vanillaCheckBox);
            this.Controls.Add(this.orangeCheckBox);
            this.Controls.Add(this.picklesCheckBox);
            this.Controls.Add(this.cheeseCheckBox);
            this.Controls.Add(this.onionsCheckBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.drinkCheckBox);
            this.Controls.Add(this.burgerCheckBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox burgerCheckBox;
        private System.Windows.Forms.CheckBox drinkCheckBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox onionsCheckBox;
        private System.Windows.Forms.CheckBox cheeseCheckBox;
        private System.Windows.Forms.CheckBox picklesCheckBox;
        private System.Windows.Forms.CheckBox orangeCheckBox;
        private System.Windows.Forms.CheckBox vanillaCheckBox;
        private System.Windows.Forms.CheckBox cherryCheckBox;
        private System.Windows.Forms.Label drinkErrorLabel;
        private System.Windows.Forms.Label burgerErrorLabel;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.TextBox orderTextBox;
        private System.Windows.Forms.Label priceLabel;
        private System.Windows.Forms.Label nameErrorLabel;
    }
}

