﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FontSelector
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            //populate listbox1
            listBox1.Items.Add("Calibri");
            listBox1.Items.Add("Impact");
            listBox1.Items.Add("Georgia");
            listBox1.Items.Add("Times New Roman");

            //populate listbox2
            listBox2.Items.Add("12");
            listBox2.Items.Add("14");
            listBox2.Items.Add("16");
            listBox2.Items.Add("18");

            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            listBox1.SelectedIndex = 0;

            this.listBox2.SelectedIndexChanged += new System.EventHandler(this.listBox2_SelectedIndexChanged);
            listBox2.SelectedIndex = 0;

            textBox1.Text = "Hello";
        }

        private void UpdateFont()
        {
            if (listBox1.SelectedIndex == -1 || listBox2.SelectedIndex == -1)
                return;  // selection not complete yet, so do nothing

            string font = listBox1.SelectedItem.ToString();
            float size = Convert.ToSingle(listBox2.SelectedItem.ToString());

            textBox1.Font = new Font(font, size);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            UpdateFont();
        }
    }
}
