﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DavesDriveways
{
    public partial class Form1 : Form
    {
        int priceOfMaterial = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            if (priceOfMaterial != 0)
            {
                if (tbSquareFt.Text != null)
                {
                    int squareFt;
                    Int32.TryParse(tbSquareFt.Text, out squareFt);
                    if (squareFt > 0)
                    {
                        lblTotal.Text = "$";
                        int doMath = squareFt * priceOfMaterial;
                        lblTotal.Visible = true;
                        lblTotal.Text += doMath.ToString();
                    }
                }
            }
        }

        private void rbGravel_CheckedChanged(object sender, EventArgs e)
        {
            priceOfMaterial = 10;
        }

        private void rbAsphalt_CheckedChanged(object sender, EventArgs e)
        {
            priceOfMaterial = 12;
        }

        private void rbCement_CheckedChanged(object sender, EventArgs e)
        {
            priceOfMaterial = 14;
        }

        private void rbBrick_CheckedChanged(object sender, EventArgs e)
        {
            priceOfMaterial = 17;
        }
    }
}
