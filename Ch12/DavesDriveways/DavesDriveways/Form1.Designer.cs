﻿namespace DavesDriveways
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.rbGravel = new System.Windows.Forms.RadioButton();
            this.rbAsphalt = new System.Windows.Forms.RadioButton();
            this.rbCement = new System.Windows.Forms.RadioButton();
            this.rbBrick = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbSquareFt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonCalculate = new System.Windows.Forms.Button();
            this.lblResult = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.label1.Location = new System.Drawing.Point(121, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(174, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Dave\'s Driveway";
            // 
            // rbGravel
            // 
            this.rbGravel.AutoSize = true;
            this.rbGravel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.rbGravel.Location = new System.Drawing.Point(6, 10);
            this.rbGravel.Name = "rbGravel";
            this.rbGravel.Size = new System.Drawing.Size(114, 24);
            this.rbGravel.TabIndex = 1;
            this.rbGravel.TabStop = true;
            this.rbGravel.Text = "Gravel ($10)";
            this.rbGravel.UseVisualStyleBackColor = true;
            this.rbGravel.CheckedChanged += new System.EventHandler(this.rbGravel_CheckedChanged);
            // 
            // rbAsphalt
            // 
            this.rbAsphalt.AutoSize = true;
            this.rbAsphalt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.rbAsphalt.Location = new System.Drawing.Point(6, 40);
            this.rbAsphalt.Name = "rbAsphalt";
            this.rbAsphalt.Size = new System.Drawing.Size(122, 24);
            this.rbAsphalt.TabIndex = 2;
            this.rbAsphalt.TabStop = true;
            this.rbAsphalt.Text = "Asphalt ($12)";
            this.rbAsphalt.UseVisualStyleBackColor = true;
            this.rbAsphalt.CheckedChanged += new System.EventHandler(this.rbAsphalt_CheckedChanged);
            // 
            // rbCement
            // 
            this.rbCement.AutoSize = true;
            this.rbCement.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.rbCement.Location = new System.Drawing.Point(6, 70);
            this.rbCement.Name = "rbCement";
            this.rbCement.Size = new System.Drawing.Size(124, 24);
            this.rbCement.TabIndex = 3;
            this.rbCement.TabStop = true;
            this.rbCement.Text = "Cement ($14)";
            this.rbCement.UseVisualStyleBackColor = true;
            this.rbCement.CheckedChanged += new System.EventHandler(this.rbCement_CheckedChanged);
            // 
            // rbBrick
            // 
            this.rbBrick.AutoSize = true;
            this.rbBrick.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.rbBrick.Location = new System.Drawing.Point(6, 100);
            this.rbBrick.Name = "rbBrick";
            this.rbBrick.Size = new System.Drawing.Size(103, 24);
            this.rbBrick.TabIndex = 4;
            this.rbBrick.TabStop = true;
            this.rbBrick.Text = "Brick ($17)";
            this.rbBrick.UseVisualStyleBackColor = true;
            this.rbBrick.CheckedChanged += new System.EventHandler(this.rbBrick_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbGravel);
            this.groupBox1.Controls.Add(this.rbBrick);
            this.groupBox1.Controls.Add(this.rbAsphalt);
            this.groupBox1.Controls.Add(this.rbCement);
            this.groupBox1.Location = new System.Drawing.Point(24, 69);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 130);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            // 
            // tbSquareFt
            // 
            this.tbSquareFt.Location = new System.Drawing.Point(234, 113);
            this.tbSquareFt.Name = "tbSquareFt";
            this.tbSquareFt.Size = new System.Drawing.Size(100, 20);
            this.tbSquareFt.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label2.Location = new System.Drawing.Point(230, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(180, 20);
            this.label2.TabIndex = 7;
            this.label2.Text = "Number of Square Feet:";
            // 
            // buttonCalculate
            // 
            this.buttonCalculate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.buttonCalculate.Location = new System.Drawing.Point(234, 139);
            this.buttonCalculate.Name = "buttonCalculate";
            this.buttonCalculate.Size = new System.Drawing.Size(100, 24);
            this.buttonCalculate.TabIndex = 8;
            this.buttonCalculate.Text = "Calculate";
            this.buttonCalculate.UseVisualStyleBackColor = true;
            this.buttonCalculate.Click += new System.EventHandler(this.buttonCalculate_Click);
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblResult.Location = new System.Drawing.Point(24, 218);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(63, 20);
            this.lblResult.TabIndex = 9;
            this.lblResult.Text = "Total is:";
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lblTotal.Location = new System.Drawing.Point(93, 218);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(0, 20);
            this.lblTotal.TabIndex = 10;
            this.lblTotal.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 320);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.buttonCalculate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbSquareFt);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbGravel;
        private System.Windows.Forms.RadioButton rbAsphalt;
        private System.Windows.Forms.RadioButton rbCement;
        private System.Windows.Forms.RadioButton rbBrick;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbSquareFt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonCalculate;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Label lblTotal;
    }
}

