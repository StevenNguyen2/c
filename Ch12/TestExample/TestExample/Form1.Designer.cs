﻿namespace TestExample
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cdRadBtn = new System.Windows.Forms.RadioButton();
            this.dvdRadBtn = new System.Windows.Forms.RadioButton();
            this.bluRayRadBtn = new System.Windows.Forms.RadioButton();
            this.greetingLbl = new System.Windows.Forms.Label();
            this.addButton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.warrantyCheckBox = new System.Windows.Forms.CheckBox();
            this.cartLabel = new System.Windows.Forms.Label();
            this.totalLabel = new System.Windows.Forms.Label();
            this.ratingLabel = new System.Windows.Forms.Label();
            this.ageLabel = new System.Windows.Forms.Label();
            this.ageTextBox = new System.Windows.Forms.TextBox();
            this.submitButton = new System.Windows.Forms.Button();
            this.errorLbl = new System.Windows.Forms.Label();
            this.lblCartNum = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // cdRadBtn
            // 
            this.cdRadBtn.AutoSize = true;
            this.cdRadBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.cdRadBtn.Location = new System.Drawing.Point(46, 50);
            this.cdRadBtn.Name = "cdRadBtn";
            this.cdRadBtn.Size = new System.Drawing.Size(54, 28);
            this.cdRadBtn.TabIndex = 0;
            this.cdRadBtn.TabStop = true;
            this.cdRadBtn.Text = "CD";
            this.cdRadBtn.UseVisualStyleBackColor = true;
            this.cdRadBtn.CheckedChanged += new System.EventHandler(this.cdRadBtn_CheckedChanged);
            // 
            // dvdRadBtn
            // 
            this.dvdRadBtn.AutoSize = true;
            this.dvdRadBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.dvdRadBtn.Location = new System.Drawing.Point(46, 91);
            this.dvdRadBtn.Name = "dvdRadBtn";
            this.dvdRadBtn.Size = new System.Drawing.Size(67, 28);
            this.dvdRadBtn.TabIndex = 1;
            this.dvdRadBtn.TabStop = true;
            this.dvdRadBtn.Text = "DVD";
            this.dvdRadBtn.UseVisualStyleBackColor = true;
            this.dvdRadBtn.CheckedChanged += new System.EventHandler(this.dvdRadBtn_CheckedChanged);
            // 
            // bluRayRadBtn
            // 
            this.bluRayRadBtn.AutoSize = true;
            this.bluRayRadBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.bluRayRadBtn.Location = new System.Drawing.Point(46, 134);
            this.bluRayRadBtn.Name = "bluRayRadBtn";
            this.bluRayRadBtn.Size = new System.Drawing.Size(92, 28);
            this.bluRayRadBtn.TabIndex = 2;
            this.bluRayRadBtn.TabStop = true;
            this.bluRayRadBtn.Text = "Blu Ray";
            this.bluRayRadBtn.UseVisualStyleBackColor = true;
            this.bluRayRadBtn.CheckedChanged += new System.EventHandler(this.bluRayRadBtn_CheckedChanged);
            // 
            // greetingLbl
            // 
            this.greetingLbl.AutoSize = true;
            this.greetingLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.greetingLbl.Location = new System.Drawing.Point(218, 9);
            this.greetingLbl.Name = "greetingLbl";
            this.greetingLbl.Size = new System.Drawing.Size(129, 26);
            this.greetingLbl.TabIndex = 3;
            this.greetingLbl.Text = "Media Store";
            // 
            // addButton
            // 
            this.addButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addButton.Location = new System.Drawing.Point(46, 178);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(115, 29);
            this.addButton.TabIndex = 4;
            this.addButton.Text = "Add To Cart";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(223, 50);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(200, 200);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // warrantyCheckBox
            // 
            this.warrantyCheckBox.AutoSize = true;
            this.warrantyCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.warrantyCheckBox.Location = new System.Drawing.Point(458, 51);
            this.warrantyCheckBox.Name = "warrantyCheckBox";
            this.warrantyCheckBox.Size = new System.Drawing.Size(143, 28);
            this.warrantyCheckBox.TabIndex = 6;
            this.warrantyCheckBox.Text = "Add Warranty";
            this.warrantyCheckBox.UseVisualStyleBackColor = true;
            // 
            // cartLabel
            // 
            this.cartLabel.AutoSize = true;
            this.cartLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.cartLabel.Location = new System.Drawing.Point(46, 263);
            this.cartLabel.Name = "cartLabel";
            this.cartLabel.Size = new System.Drawing.Size(59, 26);
            this.cartLabel.TabIndex = 7;
            this.cartLabel.Text = "Cart:";
            // 
            // totalLabel
            // 
            this.totalLabel.AutoSize = true;
            this.totalLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.totalLabel.Location = new System.Drawing.Point(46, 461);
            this.totalLabel.Name = "totalLabel";
            this.totalLabel.Size = new System.Drawing.Size(65, 26);
            this.totalLabel.TabIndex = 9;
            this.totalLabel.Text = "Total:";
            // 
            // ratingLabel
            // 
            this.ratingLabel.AutoSize = true;
            this.ratingLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.ratingLabel.Location = new System.Drawing.Point(268, 263);
            this.ratingLabel.Name = "ratingLabel";
            this.ratingLabel.Size = new System.Drawing.Size(212, 26);
            this.ratingLabel.TabIndex = 10;
            this.ratingLabel.Text = "Rated R Verification:";
            // 
            // ageLabel
            // 
            this.ageLabel.AutoSize = true;
            this.ageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ageLabel.Location = new System.Drawing.Point(268, 308);
            this.ageLabel.Name = "ageLabel";
            this.ageLabel.Size = new System.Drawing.Size(117, 20);
            this.ageLabel.TabIndex = 11;
            this.ageLabel.Text = "Enter your age:";
            // 
            // ageTextBox
            // 
            this.ageTextBox.Location = new System.Drawing.Point(391, 308);
            this.ageTextBox.Name = "ageTextBox";
            this.ageTextBox.Size = new System.Drawing.Size(100, 20);
            this.ageTextBox.TabIndex = 12;
            // 
            // submitButton
            // 
            this.submitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.submitButton.Location = new System.Drawing.Point(497, 302);
            this.submitButton.Name = "submitButton";
            this.submitButton.Size = new System.Drawing.Size(115, 29);
            this.submitButton.TabIndex = 13;
            this.submitButton.Text = "Submit";
            this.submitButton.UseVisualStyleBackColor = true;
            this.submitButton.Click += new System.EventHandler(this.submitButton_Click);
            // 
            // errorLbl
            // 
            this.errorLbl.AutoSize = true;
            this.errorLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.errorLbl.Location = new System.Drawing.Point(269, 354);
            this.errorLbl.Name = "errorLbl";
            this.errorLbl.Size = new System.Drawing.Size(51, 20);
            this.errorLbl.TabIndex = 15;
            this.errorLbl.Text = "label2";
            // 
            // lblCartNum
            // 
            this.lblCartNum.AutoSize = true;
            this.lblCartNum.Location = new System.Drawing.Point(577, 18);
            this.lblCartNum.Name = "lblCartNum";
            this.lblCartNum.Size = new System.Drawing.Size(0, 13);
            this.lblCartNum.TabIndex = 17;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(51, 310);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(164, 129);
            this.textBox1.TabIndex = 18;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(652, 607);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lblCartNum);
            this.Controls.Add(this.errorLbl);
            this.Controls.Add(this.submitButton);
            this.Controls.Add(this.ageTextBox);
            this.Controls.Add(this.ageLabel);
            this.Controls.Add(this.ratingLabel);
            this.Controls.Add(this.totalLabel);
            this.Controls.Add(this.cartLabel);
            this.Controls.Add(this.warrantyCheckBox);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.greetingLbl);
            this.Controls.Add(this.bluRayRadBtn);
            this.Controls.Add(this.dvdRadBtn);
            this.Controls.Add(this.cdRadBtn);
            this.Name = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton cdRadBtn;
        private System.Windows.Forms.RadioButton dvdRadBtn;
        private System.Windows.Forms.RadioButton bluRayRadBtn;
        private System.Windows.Forms.Label greetingLbl;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox warrantyCheckBox;
        private System.Windows.Forms.Label cartLabel;
        private System.Windows.Forms.Label totalLabel;
        private System.Windows.Forms.Label ratingLabel;
        private System.Windows.Forms.Label ageLabel;
        private System.Windows.Forms.TextBox ageTextBox;
        private System.Windows.Forms.Button submitButton;
        private System.Windows.Forms.Label errorLbl;
        private System.Windows.Forms.Label lblCartNum;
        private System.Windows.Forms.TextBox textBox1;
    }
}

