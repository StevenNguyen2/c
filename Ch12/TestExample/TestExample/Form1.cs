﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestExample
{
    public partial class Form1 : Form
    {
        int numOfItems = 0;
        int numOfCD = 0;
        int numOfDVD = 0;
        int numOfBluRay = 0;
        string typeOfItem;
        double priceOfCD = 9.99;
        double priceOfDVD = 14.99;
        double priceOfBluRay = 19.99;
        double priceOfWarranty = 0;
        string stringCDVal;
        string stringDVDVal;
        string stringBluVal;
        Boolean cdWarranty = false;
        Boolean dvdWarranty = false;
        Boolean bluRayWarranty = false;
        int cdWarrantyCount = 0;
        int dvdWarrantyCount = 0;
        int blurayWarrantyCount = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void cdRadBtn_CheckedChanged(object sender, EventArgs e)
        {
            pictureBox1.ImageLocation = "..\\..\\items\\cd.jpg";
            cdRadBtn.BackColor = Color.DarkGray;
            dvdRadBtn.BackColor = Color.LightGray;
            bluRayRadBtn.BackColor = Color.LightGray;
            typeOfItem = "CD";
        }

        private void dvdRadBtn_CheckedChanged(object sender, EventArgs e)
        {
            pictureBox1.ImageLocation = "..\\..\\items\\DVD-logo.png";
            cdRadBtn.BackColor = Color.LightGray;
            dvdRadBtn.BackColor = Color.DarkGray;
            bluRayRadBtn.BackColor = Color.LightGray;
            typeOfItem = "DVD";
        }

        private void bluRayRadBtn_CheckedChanged(object sender, EventArgs e)
        {
            pictureBox1.ImageLocation = "..\\..\\items\\blu-ray-icon-26.png";
            cdRadBtn.BackColor = Color.LightGray;
            dvdRadBtn.BackColor = Color.LightGray;
            bluRayRadBtn.BackColor = Color.DarkGray;
            typeOfItem = "Blu Ray";
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            int age = Convert.ToInt32(ageTextBox.Text);
            try
            {
                if (age < 18)
                    throw new AgeException("You are not old enough");
                else
                {
                    errorLbl.Text = "You are old enough";
                }
            }
            catch(AgeException error)
            {
                errorLbl.Text = "You aren't old enough";
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            numOfItems++;
            lblCartNum.Text = "Cart: " + numOfItems.ToString();

            switch (typeOfItem) //Purpose is to increment quantity to cart
            {
                case "CD":
                    numOfCD++;
                    priceOfWarranty = 1.99;
                    stringCDVal = numOfCD + " " + "CD $" + (numOfCD * priceOfCD) + "\r\n";
                    if (cdWarranty == true)
                    {
                        cdWarranty = true;
                        cdWarrantyCount++;
                        stringCDVal += cdWarrantyCount + " CD Warranties: " + " $" + (cdWarrantyCount * priceOfWarranty).ToString() + "\r\n";
                    }
                    else if(warrantyCheckBox.Checked == true)
                    {
                        cdWarrantyCount++;
                        stringCDVal += cdWarrantyCount + " CD Warranties:" + " $" + (cdWarrantyCount * priceOfWarranty).ToString() + "\r\n";
                    }
                    break;
                case "DVD":
                    numOfDVD++;
                    priceOfWarranty = 2.99;
                    stringDVDVal = numOfDVD + " " + "DVD $" + (numOfDVD * priceOfCD) + "\r\n";
                    if (dvdWarranty == true)
                    {
                        dvdWarranty = true;
                        dvdWarrantyCount++;
                        stringDVDVal += dvdWarrantyCount + " DVD Warranties:" + " $" + (dvdWarrantyCount * priceOfWarranty).ToString() + "\r\n";
                    }
                    else if (warrantyCheckBox.Checked == true)
                    {
                        dvdWarrantyCount++;
                        stringDVDVal += dvdWarrantyCount + " DVD Warranties:" + " $" + (dvdWarrantyCount * priceOfWarranty).ToString() + "\r\n";
                    }
                    break;
                case "Blu Ray":
                    numOfBluRay++;
                    priceOfWarranty = 3.99;
                    stringBluVal = numOfBluRay + " " + "Blu Ray $" + (numOfBluRay * priceOfBluRay) + "\r\n";
                    if (bluRayWarranty == true)
                    {
                        bluRayWarranty = true;
                        blurayWarrantyCount++;
                        stringBluVal += blurayWarrantyCount + " Blu Ray Warranties:" + " $" + (blurayWarrantyCount * priceOfWarranty).ToString() + "\r\n";
                    } else if (warrantyCheckBox.Checked == true)
                    {
                        blurayWarrantyCount++;
                        stringBluVal += blurayWarrantyCount + " Blu Ray Warranties:" +  " $" + (blurayWarrantyCount * priceOfWarranty).ToString() + "\r\n";
                    }
                    break;
            }

            double cdTotal = numOfCD * priceOfCD;
            double dvdTotal = numOfDVD * priceOfDVD;
            double blurayTotal = numOfBluRay * priceOfBluRay;
            double warrantyTotal = 0;

            if (warrantyCheckBox.Checked == true)
            {
                warrantyTotal += priceOfWarranty;
                numOfItems++;
                lblCartNum.Text = "Cart: " + numOfItems.ToString();
            }
            textBox1.Text = stringCDVal + "\r\n" + stringDVDVal + "\r\n" + stringBluVal;


            totalLabel.Text = "Total: $" + (cdTotal + dvdTotal + blurayTotal + warrantyTotal).ToString();

            warrantyCheckBox.Checked = false; //clear checkbox
        }
    }
}
