﻿namespace FoodOrder
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblName = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.lblErrorName = new System.Windows.Forms.Label();
            this.lblSundae = new System.Windows.Forms.Label();
            this.lblSoda = new System.Windows.Forms.Label();
            this.cbSundaeYes = new System.Windows.Forms.CheckBox();
            this.cbSodaYes = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbSprinkles = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(13, 23);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(189, 13);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "What name do you want on the order?";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(13, 40);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(189, 20);
            this.tbName.TabIndex = 1;
            // 
            // lblErrorName
            // 
            this.lblErrorName.AutoSize = true;
            this.lblErrorName.Location = new System.Drawing.Point(16, 67);
            this.lblErrorName.Name = "lblErrorName";
            this.lblErrorName.Size = new System.Drawing.Size(35, 13);
            this.lblErrorName.TabIndex = 2;
            this.lblErrorName.Text = "label1";
            // 
            // lblSundae
            // 
            this.lblSundae.AutoSize = true;
            this.lblSundae.Location = new System.Drawing.Point(13, 112);
            this.lblSundae.Name = "lblSundae";
            this.lblSundae.Size = new System.Drawing.Size(120, 13);
            this.lblSundae.TabIndex = 3;
            this.lblSundae.Text = "Do you want a sundae?";
            // 
            // lblSoda
            // 
            this.lblSoda.AutoSize = true;
            this.lblSoda.Location = new System.Drawing.Point(194, 112);
            this.lblSoda.Name = "lblSoda";
            this.lblSoda.Size = new System.Drawing.Size(108, 13);
            this.lblSoda.TabIndex = 4;
            this.lblSoda.Text = "Do you want a soda?";
            // 
            // cbSundaeYes
            // 
            this.cbSundaeYes.AutoSize = true;
            this.cbSundaeYes.Location = new System.Drawing.Point(16, 138);
            this.cbSundaeYes.Name = "cbSundaeYes";
            this.cbSundaeYes.Size = new System.Drawing.Size(44, 17);
            this.cbSundaeYes.TabIndex = 5;
            this.cbSundaeYes.Text = "Yes";
            this.cbSundaeYes.UseVisualStyleBackColor = true;
            // 
            // cbSodaYes
            // 
            this.cbSodaYes.AutoSize = true;
            this.cbSodaYes.Location = new System.Drawing.Point(197, 138);
            this.cbSodaYes.Name = "cbSodaYes";
            this.cbSodaYes.Size = new System.Drawing.Size(44, 17);
            this.cbSodaYes.TabIndex = 6;
            this.cbSodaYes.Text = "Yes";
            this.cbSodaYes.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 187);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Do you want a sundae?";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(194, 187);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Do you want a sundae?";
            // 
            // cbSprinkles
            // 
            this.cbSprinkles.AutoSize = true;
            this.cbSprinkles.Location = new System.Drawing.Point(16, 215);
            this.cbSprinkles.Name = "cbSprinkles";
            this.cbSprinkles.Size = new System.Drawing.Size(69, 17);
            this.cbSprinkles.TabIndex = 9;
            this.cbSprinkles.Text = "Sprinkles";
            this.cbSprinkles.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(16, 261);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(69, 17);
            this.checkBox1.TabIndex = 10;
            this.checkBox1.Text = "Sprinkles";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(16, 238);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(69, 17);
            this.checkBox2.TabIndex = 11;
            this.checkBox2.Text = "Sprinkles";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 371);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.cbSprinkles);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbSodaYes);
            this.Controls.Add(this.cbSundaeYes);
            this.Controls.Add(this.lblSoda);
            this.Controls.Add(this.lblSundae);
            this.Controls.Add(this.lblErrorName);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.lblName);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label lblErrorName;
        private System.Windows.Forms.Label lblSundae;
        private System.Windows.Forms.Label lblSoda;
        private System.Windows.Forms.CheckBox cbSundaeYes;
        private System.Windows.Forms.CheckBox cbSodaYes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox cbSprinkles;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
    }
}

