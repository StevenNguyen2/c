﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvertQuartsToLiters
{
    class Program
    {
        //Create an application named ConvertQuartsToLiters whose Main() method prompts a user for a number of quarts, 
        //passes the value to a method that converts the value to liters, 
        //and then returns the value to the Main() method where it is displayed.A quart is 0.966353 liters.
        static void Main(string[] args)
        {
            double quarts;
            double theTotal;

            WriteLine("Enter a number of quarts: ");
            quarts = Convert.ToDouble(ReadLine());

            theTotal = ConvertQtToLiter(quarts);
            WriteLine("Amount of liters: " + theTotal.ToString());
            ReadLine();
        }

        private static double ConvertQtToLiter(double quart)
        {
            double calcLiter;
            calcLiter = quart * 0.966353;
            return calcLiter;
        }
    }
}
