﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multiplication
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("Input a number");
            int integer = Convert.ToInt32(ReadLine());
            DisplayTable(integer);
            ReadLine();
        }

        static int DisplayTable(int user)
        {
            for (int i = 2; i < 11; i++)
            {
                WriteLine(user + " * " + i + " = " + user * i);
            }
            return user;
        }
    }
}
