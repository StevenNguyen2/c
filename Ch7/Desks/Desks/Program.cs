﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace Desks
{
    class Program
    {
        static void Main(string[] args)
        {
            int drawers = numDrawers();
            char wood = Wood();
            int totalCost = getCost(drawers, wood);
            displayDetails(drawers, wood, totalCost);
            ReadLine();
        }

        static int numDrawers()
        {
            WriteLine("Enter # of drawers");
            int drawers = Convert.ToInt32(ReadLine());
            return drawers;
        }

        static char Wood()
        {
            WriteLine("Enter M for Mahogany, O for Oak, or P for Pine.");
            char wood = Convert.ToChar(ReadLine());
            return wood;
        }

        static int getCost(int drawer, char wood)
        {
            int totalCost = 0;
            totalCost += drawer * 30;
            if (wood == 'p')
            {
                totalCost += 100;
            }
            else if (wood == 'o')
            {
                totalCost += 140;
            }
            else
            {
                totalCost += 180;
            }
            return totalCost;
        }

        static void displayDetails(int drawers, char wood, int total)
        {
            string woodType;
            if (wood == 'p')
            {
                woodType = "Pine";
            }
            else if (wood == 'o')
            {
                woodType = "Oak";
            }
            else if (wood == 'm')
            {
                woodType = "Mahogany";
            }
            else
            {
                woodType = "Other";
            }

            WriteLine("Type: " + woodType);
            WriteLine("# of drawers: " + drawers);
            WriteLine("Total: " + total.ToString("C"));
        }
    }
}