﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FortuneTeller
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create a program named FortuneTeller whose Main() method contains an array of at least six strings with fortune - 
            //telling phrases such as I see a tall, dark stranger in your future. The program randomly selects two different fortunes 
            //and passes them to a method that displays them.
            string[] fortune = { "dope", "fire", "flame", "water", "hey", "dud"};
            SelectTwoFortunes(fortune);
            ReadLine();
        }

        private static void SelectTwoFortunes(string[] fortunes)
        {
            Random random = new Random();

            //Selects random number
            int index = random.Next(0, 6);
            int index2 = random.Next(0, 6);

            //Grabs the index of random number generated above
            string num1 = fortunes[index];
            string num2 = fortunes[index2];

            //Makes sure the two phrases are different
            if (index == index2)
            {
                if(index2 == 6)
                {
                    index2 -= 1;
                }
                else
                {
                    index += 1;
                }
            }

            //Display the two different phrases
            WriteLine(num1);
            WriteLine(num2);
        }
    }
}
