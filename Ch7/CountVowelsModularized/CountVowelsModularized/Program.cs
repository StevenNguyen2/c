﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountVowelsModularized
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("Phrase:");
            string s = ReadLine();
            WriteLine("# of vowels: " + getNumberOfVowels(s));
            ReadLine();
        }

        static int getNumberOfVowels(String s)
        {
            int count = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] == 'A' || s[i] == 'E' || s[i] == 'I' || s[i] == 'O' || s[i] == 'U' || s[i] == 'a' || s[i] == 'e' || s[i] == 'i' || s[i] == 'o' || s[i] == 'u')
                {
                    count++;
                }
            }
            return count;
        }
    }
}
