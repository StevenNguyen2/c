﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaintingExercise
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create a program named PaintingEstimate whose Main() method prompts a user for length and width of a room in feet.
            //Create a method that accepts the values and then computes the cost of painting the room,
            //assuming the room is rectangular and has four full walls and 9 - foot ceilings.
            //The price of the job is $6 per square foot.Return the price to the Main() method, and display it.
            int myLength,
                myWidth,
                theTotal;

            Write("Enter Length: ");
            myLength = Convert.ToInt32(ReadLine());
            Write("Enter Width: ");
            myWidth = Convert.ToInt32(ReadLine());
            theTotal = calcCost(myLength, myWidth);
            WriteLine("The total price: " + theTotal.ToString("C"));
            ReadLine();
        }

        private static int calcCost(int length, int width)
        {
            int price;
            const int squareFt = 6;
            const int height = 2;
            price = ((length * height) * 2 + (width * height) * 2) * 6;
            return price;
        }
    }
}
