﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PayrollGUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double payrate,
                grossPay,
                fedTax,
                stateTax,
                netPay;

            int hours;

            payrate = Convert.ToDouble(textBox2.Text);
            hours = Convert.ToInt32(textBox3.Text);

            grossPay = payrate * hours;
            fedTax = grossPay * 0.15;
            stateTax = grossPay * 0.05;
            netPay = grossPay - (fedTax + stateTax);

            textBox4.Text = grossPay.ToString("C");
            textBox5.Text = fedTax.ToString("C");
            textBox6.Text = stateTax.ToString("C");
            textBox7.Text = netPay.ToString("C");
        }
    }
}
