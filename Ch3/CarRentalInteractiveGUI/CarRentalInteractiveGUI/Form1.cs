﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarRentalInteractiveGUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int miles;
            int days;
            miles = Convert.ToInt32(textBox1.Text);
            days = Convert.ToInt32(textBox2.Text);
            double calc = (miles * 0.25) + (days * 20);
            textBox3.Text = calc.ToString();
        }
    }
}
