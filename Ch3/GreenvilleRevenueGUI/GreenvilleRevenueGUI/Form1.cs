﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GreenvilleRevenueGUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double lastYr = Convert.ToDouble(textBox1.Text);
            double thisYr = Convert.ToDouble(textBox2.Text);
            double calc1 = lastYr * 25;
            double calc2 = thisYr * 25;

            if (calc2 > calc1)
            {
                label3.Text = "This year has more contestants";
            } else
            {
                label3.Text = "This year does not have more contestants";
            }
        }
    }
}
