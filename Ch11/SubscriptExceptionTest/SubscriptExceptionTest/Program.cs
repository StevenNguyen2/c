﻿using System;
using static System.Console;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubscriptExceptionTest
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] newArray = new double[20] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };
            int input;

            for (int i = 0; i < newArray.Length; ++i)
            {
                try
                {
                    WriteLine("Enter a value that corresponds to the position of a number in the array");
                    input = Convert.ToInt32(ReadLine());
                    WriteLine(newArray[input]);
                }
                catch (IndexOutOfRangeException e)
                {
                    WriteLine("What you entered is not a value in the array");
                }
            }
            ReadLine();
        }
    }
}
