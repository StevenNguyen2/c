﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderExceptionDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            CreateOrder(923, 10, 23);
            CreateOrder(213, 14, 3);
            CreateOrder(3, 8, 30);
            CreateOrder(342, 1, 34);
            CreateOrder(314, 10, 12);
            CreateOrder(999, 100, 100);
            Console.ReadLine();
        }

        private static void CreateOrder(int itemNum, int quantity, int day)
        {
            try
            {
                Order order1 = new Order(itemNum, quantity, day);
                Console.WriteLine(order1.ToString());
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
    public class Order
    {
        public Order(int itemNum, int quantity, int dayOrdered)
        {
            if (itemNum > 999 || itemNum < 100)
            {
                throw new ArgumentException("Item # is INVALID ");
            }
            if (quantity < 1 || quantity > 12)
            {
                throw new ArgumentException("Amount = INVALID ");
            }
            if (dayOrdered < 1 || dayOrdered > 31)
            {
                throw new ArgumentException("Day = INVALID ");
            }

            ItemNum = itemNum;
            Quantity = quantity;
            DayOrdered = dayOrdered;
        }

        public int ItemNum;

        public int Quantity { get; set; }

        public int DayOrdered { get; set; }

        public override string ToString()
        {
            return $"Item Number: {ItemNum}  Amount: {Quantity}  Day Ordered: {DayOrdered}";
        }
    }
}
