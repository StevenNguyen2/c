﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookExceptionDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Book[] b = new Book[4];
            double price = 0, numPages = 0;
            string title, sPrice, sNumPages;
            int count = 0;

            do
            {
                bool result = false;

                Console.Write("Enter title: ");
                title = Console.ReadLine();

                while (result == false)
                {
                    Console.Write("Enter price: ");
                    sPrice = Console.ReadLine();
                    result = Double.TryParse(sPrice, out price);

                    if (result == false)
                    {
                        Console.WriteLine("Invalid. Try again.");
                        Console.WriteLine();
                    }
                }

                result = false; //reset result
                while (result == false)
                {
                    Console.Write("Enter # of pages: ");
                    sNumPages = Console.ReadLine();
                    result = Double.TryParse(sNumPages, out numPages);

                    if (result == false)
                    {
                        Console.WriteLine("Invalid. Try again.");
                        Console.WriteLine();
                    }
                }

                Console.WriteLine();

                b[count] = new Book(title, price, numPages);

                if (b[count].TestRatio())
                {
                    ++count;
                }
            } while (count < 4);

            foreach (Book element in b)
            {
                Console.WriteLine(element.ToString());
            }
        }
    }

    class BookException : Exception
    {
        public BookException(string title, double price, double numPages)
        {
            double ratio = numPages * .1;

            try
            {
                if (ratio >= price)
                {
                    throw (new Exception(String.Format("For {0}, ratio is invalid. Price is {1} for {2} pages.", title, price.ToString("C"), numPages.ToString())));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }

    class Book : BookException
    {
        private string title;
        private double price;
        private double numPages;

        public Book(string title, double price, double numPages) : base(title, price, numPages)
        {
            this.title = title;
            this.price = price;
            this.numPages = numPages;
        }

        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                this.title = value;
            }
        }

        public double Price
        {
            get
            {
                return price;
            }
            set
            {
                this.price = value;
            }
        }

        public double NumPages
        {
            get
            {
                return numPages;
            }
            set
            {
                this.numPages = value;
            }
        }

        public bool TestRatio()
        {
            bool result = true;
            double ratio = numPages * .1;

            if (ratio >= price)
            {
                result = false;
            }

            return result;
        }

        public override string ToString()
        {
            return String.Format("For {0}, the price is {1} for {2} pages.", title, price.ToString("C"), numPages.ToString());
        }
    }
}
