﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MortgageApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            string appName;
            int creditScore;

            while (true)
            {
                try
                {
                    Console.WriteLine("Name:  ");
                    appName = Console.ReadLine();

                    Console.WriteLine("Credit Score:  ");
                    creditScore = Convert.ToInt32(Console.ReadLine());
                    bool check = CheckCreditScore(creditScore);
                    if (check)
                    {
                        Console.WriteLine("You have been accepted! :D");
                    }
                    else
                    {
                        Console.WriteLine("You have been rejected :'(");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        public static bool CheckCreditScore(int creditScore)
        {
            if (creditScore < 300 || creditScore > 850)
            {
                throw new ArgumentException("Please enter a number btwn 300 and 850");
            }

            return creditScore >= 650;
        }
    }
}
