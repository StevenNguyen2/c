﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindSquareRoot
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a number to get square root:");
            double input;
            double sqroot = 0;

            try
            {
                input = Convert.ToDouble(Console.ReadLine());
                if (input >= 0)
                {
                    sqroot = Math.Sqrt(input);
                }
                else
                {
                    sqroot = 0;
                    throw new ApplicationException("Number can't be negative");
                }
            }
            catch (ApplicationException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                sqroot = 0;
                Console.WriteLine("Enter a #");

            }

            Console.WriteLine(sqroot);
            Console.ReadLine();
        }
    }
}
