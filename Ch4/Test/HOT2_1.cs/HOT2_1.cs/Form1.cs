﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HOT2_1.cs
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double magnitude = Convert.ToDouble(textBox1.Text);

            if (magnitude >= 1.0 && magnitude <= 1.9)
            {
                textBox2.Text = "Micro";
                return;
            }

            if (magnitude >= 2.0 && magnitude <= 3.9)
            {
                textBox2.Text = "Minor";
                return;
            }

            if (magnitude >= 4.0 && magnitude <= 4.9)
            {
                textBox2.Text = "Light";
                return;
            }

            if (magnitude >= 5.0 && magnitude <= 5.9)
            {
                textBox2.Text = "Moderate";
                return;
            }

            if (magnitude >= 6.0 && magnitude <= 6.9)
            {
                textBox2.Text = "Strong";
                return;
            }

            if (magnitude >= 7.0 && magnitude <= 7.9)
            {
                textBox2.Text = "Major";
                return;
            }

            if (magnitude >= 8.0)
            {
                textBox2.Text = "Great";
                return;
            }
        }
    }
}
