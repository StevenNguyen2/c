﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lawn2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int length = Convert.ToInt32(textBox1.Text);
            int width = Convert.ToInt32(textBox2.Text);
            double weekCalc = length * width;

            int calc1 = 25 * 20;
            int calc2 = 35 * 20;
            int calc3 = 50 * 20;

            int payment = Convert.ToInt32(textBox5.Text);

            if (weekCalc <= 400)
            {
                textBox3.Text = "$25";
                textBox4.Text = "$" + calc1.ToString();
            }

            if (weekCalc >= 400 && weekCalc <= 600)
            {
                textBox3.Text = "$35";
                textBox4.Text = "$" + calc2.ToString();
            }

            if (weekCalc >= 600)
            {
                textBox3.Text = "50";
                textBox4.Text = "$" + calc3.ToString();
            }

            if (payment == 1)
            {
                textBox6.Text = "1 Payment";
                if (weekCalc <= 400)
                {
                    textBox7.Text = calc1.ToString();
                    textBox8.Text = calc1.ToString();
                }
                if (weekCalc >= 400 && weekCalc <= 600)
                {
                    textBox7.Text = calc2.ToString();
                    textBox8.Text = calc2.ToString();
                }
                if (weekCalc >= 600)
                {
                    textBox7.Text = calc3.ToString();
                    textBox8.Text = calc3.ToString();
                }
                return;
            }

            if (payment == 2)
            {
                textBox6.Text = "2 Payments";
                if (weekCalc <= 400)
                {
                    int paymentAmount3 = 500 / 2;
                    int paymentFiveOnce = 500 + 10;
                    textBox7.Text = paymentAmount3.ToString();
                    textBox8.Text = paymentFiveOnce.ToString();
                }
                if (weekCalc >= 400 && weekCalc <= 600)
                {
                    int paymentAmount2 = 700 / 2;
                    int paymentFiveAgain = 700 + 10;
                    textBox7.Text = paymentAmount2.ToString();
                    textBox8.Text = paymentFiveAgain.ToString();
                }
                if (weekCalc >= 600)
                {
                    int paymentAmount = (1000 / 2);
                    int paymentFive = 1000 + 10;
                    textBox7.Text = paymentAmount.ToString();
                    textBox8.Text = paymentFive.ToString();
                }
            }

            if (payment == 3)
            {
                textBox6.Text = "3 Payments";
                if (weekCalc <= 400)
                {
                    int payment3 = 500 / 3;
                    int payment3Again = 500 + 60;
                    textBox7.Text = payment3.ToString();
                    textBox8.Text = payment3Again.ToString();
                }
                if (weekCalc >= 400 && weekCalc <= 600)
                {
                    int payment4 = 700 / 3;
                    int payment4Again = 700 + 60;
                    textBox7.Text = payment4.ToString();
                    textBox8.Text = payment4Again.ToString();
                }
                if (weekCalc >= 600)
                {
                    int payment5 = 1000 / 3;
                    int payment5Again = 1000 + 60;
                    textBox7.Text = payment5.ToString();
                    textBox8.Text = payment5Again.ToString();
                }
            }
        }
    }
}
