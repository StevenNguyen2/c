﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lawn
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int length = Convert.ToInt32(textBox1.Text);
            int width = Convert.ToInt32(textBox2.Text);

            double weekCalc = length * width;
            //double totalCalc = weekCalc * 20;
            //textBox3.Text = weekCalc.ToString();

            if (weekCalc <= 400)
            {
                textBox3.Text = "$25";
                int calc1 = 25 * 20;
                textBox4.Text = "$" + calc1.ToString();
                return;
            }

            if (weekCalc >= 400 && weekCalc <= 600)
            {
                textBox3.Text = "$35";
                int calc2 = 35 * 20;
                textBox4.Text = "$" + calc2.ToString();
                return;
            }

            if (weekCalc >= 600)
            {
                textBox3.Text = "50";
                int calc3 = 50 * 20;
                textBox4.Text = "$" + calc3.ToString();
                return;
            }
        }
    }
}
