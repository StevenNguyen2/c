﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HOT2_2.cs
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double grade = Convert.ToDouble(textBox1.Text);

            if (grade > 3.0 && grade <= 4.0)
            {
                textBox2.Text = "A";
                return;
            }

            if (grade > 2.0 && grade <= 3.0)
            {
                textBox2.Text = "B";
                return;
            }

            if (grade > 1.0 && grade <= 2.0)
            {
                textBox2.Text = "C";
                return;
            }

            if (grade >= 0.1 && grade <= 1.0)
            {
                textBox2.Text = "D";
            }

            if (grade == 0)
            {
                textBox2.Text = "F";
            }
        }
    }
}
