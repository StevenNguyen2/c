﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Twitter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string msg;
            msg = Convert.ToString(textBox1.Text);

            if(msg.Length > 140)
            {
                label2.Text = "Twitter does not accept messages of more than 140 characters";
            }
            else
            {
                label2.Text = "Twitter accepts your message";
            }
        }
    }
}
