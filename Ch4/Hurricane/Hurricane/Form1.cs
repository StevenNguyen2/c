﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hurricane
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int windSpeed = Convert.ToInt32(textBox1.Text);

            if (windSpeed >= 157)
            {
                textBox2.Text = "5";
                return;
            }

            if (windSpeed >= 130)
            {
                textBox2.Text = "4";
                return;
            }
        }
    }
}
