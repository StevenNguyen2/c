﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lottery
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Declare and Initialize the 3 guesses
            int guess1 = Convert.ToInt32(textBox1.Text);
            int guess2 = Convert.ToInt32(textBox2.Text);
            int guess3 = Convert.ToInt32(textBox3.Text);

            //Declare and Initialize the 3 randomly generated numbers
            //Random randomNum = new Random();
            //int ranNum1 = randomNum.Next(1, 5);
            //int ranNum2 = randomNum.Next(1, 5);
            //int ranNum3 = randomNum.Next(1, 5);
            int ranNum1 = 1;
            int ranNum2 = 2;
            int ranNum3 = 3;
            label2.Text = "The randomly generated numbers are: " + ranNum1 + " , " + ranNum2 + " , " + ranNum3;
            label4.Text = "Your guessed numbers are: " + guess1 + " , " + guess2 + " , " + guess3;
            //Checks for all 3 being right 
            if (guess1 == ranNum1 && guess2 == ranNum2 && guess3 == ranNum3)
            {
                //all three right and in order
                label3.Text = "Your prize: $10,000";
                return;
            }

            if ((guess1 == ranNum1 || guess1 == ranNum2 || guess1 == ranNum3) && (guess2 == ranNum1 || guess2 == ranNum2 || guess2 == ranNum3) && (guess3 == ranNum1 || guess3 == ranNum2 || guess3 == ranNum3))
            {
                //all three right but not in order                
                label3.Text = "Your prize: $1,000";
                return;
            }

            //checks for 2 being right 
            if (guess1 == ranNum1 || guess1 == ranNum2 || guess1 == ranNum3)
            {
                //one right
                if (guess2 == ranNum1 || guess2 == ranNum2 || guess2 == ranNum3)
                {
                    //two right
                    label3.Text = "Your prize: $100";
                    return;

                }
                else if (guess3 == ranNum1 || guess3 == ranNum2 || guess3 == ranNum3)
                {
                    //two right
                    label3.Text = "Your prize: $100";
                    return;
                }

            }
            //if guess one is not a match check for 2 being a match 
            else if (guess2 == ranNum1 || guess2 == ranNum2 || guess2 == ranNum3)
            {
                //one right
                if (guess3 == ranNum1 || guess3 == ranNum2 || guess3 == ranNum3)
                {
                    //two right
                    label3.Text = "Your prize: $100";
                    return;
                }
            }
            if (guess1 == ranNum1 || guess1 == ranNum2 || guess1 == ranNum3 || guess2 == ranNum1 || guess2 == ranNum2 || guess2 == ranNum3 || guess3 == ranNum1 || guess3 == ranNum2 || guess3 == ranNum3)
            {
                //one right 
                label3.Text = "Your prize: $10";
                return;
            }
            else
            {
                //none right
                label3.Text = "Your prize: $0";
                return;
            }
        }
    }
}